<?php
/**
 *
 */
class follow
{
  public $follower;
  function __construct($follower)  {
    $this->follower=$follower;
  }
  public function addfollow($username)
  {
    $db = Db::getInstance();
    $req = $db->prepare('INSERT INTO `follow`(`username`, `follower`) VALUES (:username,:follower)');
    $req->execute(array('username' => $username,
                        'follower'=>$_SESSION['username']));
  }

  public function unfollow($username)
  {
    $db = Db::getInstance();
    $req = $db->prepare('DELETE FROM `follow` WHERE username=:username AND follower=:follower');
    $req->execute(array('username' => $username,
                        'follower'=>$_SESSION['username']));
  }
  public static function getfollower($username)
  {
    $db = Db::getInstance();
    $req = $db->prepare('SELECT * FROM `follow` WHERE username=:username');
    $req->execute(array('username'=>$username));
    return $req->rowCount();
  }
  public static function getfollowing($username)
  {
    $db = Db::getInstance();
    $req = $db->prepare('SELECT * FROM `follow` WHERE follower=:username');
    $req->execute(array('username'=>$username));
    return $req->rowCount();
  }


}

 ?>
