<?php
  class Post {
    // we define 3 attributes
    // they are public so that we can access them using $post->author directly
    public $id;
    public $author;
    public $content;
    public $title;
    public $cetagory;
    public $img;
    public $time;
    public $likes;

    public function __construct($id, $author,$title, $content,$img,$cetagory,$time,$likes) {
      $this->id      = $id;
      $this->author  = $author;
      $this->content = $content;
      $this->title    = $title;
      $this->cetagory = $cetagory;
      $this->img = $img;
      $this->time = $time;
      $this->likes = $likes;

    }
    public function read($post_id)    {
      $db = Db::getInstance();
        if(isset($_SESSION['username'])){
      $req=$db->prepare("SELECT * FROM `readed` WHERE post_id=:post_id AND user=:user");
      $req->execute($reqlist = array('post_id' => $post_id,
                                    'user'=>$_SESSION['user_id']));
      if($req->rowCount()<'1'){
      $req=$db->prepare("INSERT INTO `readed`(`post_id`, `user`) VALUES (:post_id,:user)");

        $req->execute($reqlist = array('post_id' => $post_id,
                                    'user'=>$_SESSION['user_id']));
        }
      }
    }
    public static function noti()
    {
      $list = [];
      $db = Db::getInstance();
      $follower=$_SESSION['username'];
      $req = $db->prepare("SELECT * FROM posts WHERE (author IN (SELECT  `username` FROM `follow` WHERE follower=:follower)) AND id NOT IN (SELECT `post_id` FROM `readed` WHERE user=:user_id) ORDER BY id DESC");
      $req->execute(array('follower' => $_SESSION['username'],
                                'user_id'=>$_SESSION['user_id']));
      foreach($req->fetchAll() as $post) {
        $cou = $db->prepare("SELECT * FROM likes WHERE post_id=:post_id");
        $cou->execute(array('post_id' => $post['id']));
        $count = $cou->rowCount();
        $list[] = new Post($post['id'], $post['author'],$post['title'], $post['content'],$post['img'],$post['cetagory'],$post['time'],$count);
      }

      return $list;
    }
    public static function allfollow() {
      $list = [];
      $db = Db::getInstance();
      $follower=$_SESSION['username'];
      $req = $db->prepare("SELECT * FROM posts WHERE (author IN (SELECT  `username` FROM `follow` WHERE follower=:follower)) ORDER BY id DESC");
      $req->execute($reqlist = array('follower' => $_SESSION['username']));

      // we create a list of Post objects from the database results
      foreach($req->fetchAll() as $post) {
        $cou = $db->prepare("SELECT * FROM likes WHERE post_id=:post_id");
        $cou->execute(array('post_id' => $post['id']));
        $count = $cou->rowCount();
        $list[] = new Post($post['id'], $post['author'],$post['title'], $post['content'],$post['img'],$post['cetagory'],$post['time'],$count);
      }

      return $list;
    }
    public static function allpost() {
      $list = [];
      $db = Db::getInstance();
      $req = $db->prepare("SELECT * FROM posts ORDER BY id DESC");
      $req->execute();

      // we create a list of Post objects from the database results
      foreach($req->fetchAll() as $post) {
        $cou = $db->prepare("SELECT * FROM likes WHERE post_id=:post_id");
        $cou->execute(array('post_id' => $post['id']));
        $count = $cou->rowCount();
        $list[] = new Post($post['id'], $post['author'],$post['title'], $post['content'],$post['img'],$post['cetagory'],$post['time'],$count);
      }

      return $list;
    }
    public static function searchpost($search) {
      $list = [];
      $db = Db::getInstance();
      $req = $db->prepare("SELECT * FROM posts WHERE author LIKE '%$search%' or title LIKE '%$search%' or content LIKE '%$search%' ORDER BY id DESC");
      $req->execute();

      // we create a list of Post objects from the database results
      foreach($req->fetchAll() as $post) {
        $cou = $db->prepare("SELECT * FROM likes WHERE post_id=:post_id");
        $cou->execute(array('post_id' => $post['id']));
        $count = $cou->rowCount();
        $list[] = new Post($post['id'], $post['author'],$post['title'], $post['content'],$post['img'],$post['cetagory'],$post['time'],$count);
      }

      return $list;
    }
public static function profile($username) {
        $list = [];

        $db = Db::getInstance();
        $req = $db->prepare('SELECT * FROM posts WHERE author =:author ORDER BY id DESC');
        if($username=='myprofile'){
        $req->execute(array('author' =>$_SESSION['username'] ));
      }else {
        $req->execute(array('author' =>$username ));

      }
        // we create a list of Post objects from the database results
        foreach($req->fetchAll() as $post) {
          $cou = $db->prepare("SELECT * FROM likes WHERE post_id=:post_id");
          $cou->execute(array('post_id' => $post['id']));
          $count = $cou->rowCount();
          $list[] = new Post($post['id'], $post['author'],$post['title'], $post['content'],$post['img'],$post['cetagory'],$post['time'],$count);
        }

        return $list;
      }

    public static function feedcategory($cetagory) {
      $list = [];
      $db = Db::getInstance();
      $req = $db->prepare('SELECT * FROM posts WHERE cetagory =:cetagory ORDER BY id DESC');
      $req->execute(array('cetagory' =>$cetagory ));
      // we create a list of Post objects from the database results
      foreach($req->fetchAll() as $post) {
        $cou = $db->prepare("SELECT * FROM likes WHERE post_id=:post_id");
        $cou->execute(array('post_id' => $post['id']));
        $count = $cou->rowCount();
        $list[] = new Post($post['id'], $post['author'],$post['title'], $post['content'],$post['img'],$post['cetagory'],$post['time'],$count);
      }

      return $list;
    }

    public static function find($id) {
      $db = Db::getInstance();
      // we make sure $id is an integer
      $id = intval($id);
      $req = $db->prepare('SELECT * FROM posts WHERE id = :id ');
      // the query was prepared, now we replace :id with our actual $id value
      $req->execute(array('id' => $id));
      $post = $req->fetch();
      $cou = $db->prepare("SELECT * FROM likes WHERE post_id=:post_id");
      $cou->execute(array('post_id' => $post['id']));
      $count = $cou->rowCount();
      return new Post($post['id'], $post['author'],$post['title'], $post['content'],$post['img'],$post['cetagory'],$post['time'],$count);
    }
    public static  function getimgProfile($username)
    {
      if($username=='myprofile'){
        $username = $_SESSION['username'];
      }
      $db = Db::getInstance();
      $req = $db->prepare('SELECT img FROM members WHERE username = :username ');
      $req->execute(array('username' => $username));
      $user = $req->fetch();
      return $user['img'];
    }
    public function addpost($author,$title,$content,$img,$cetagory){
      $db = Db::getInstance();
      $qry = "INSERT INTO `posts`( `author`,`title`, `content`,`img`,`cetagory`) VALUES (:author,:title,:content,:img,:cetagory)";
      $result = $db->prepare($qry);
      $exec = $result->execute(array(":author"=>$author,
                                        ":title"=>$title,
                                        ":content"=>$content,
                                        ":img"=>$img,
                                        ":cetagory"=>$cetagory));
    $target_dir = "img/";
      $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
      $uploadOk = 1;
      $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
      // Check if image file is a actual image or fake image
      if(isset($_POST["submit"])) {
          $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
          if($check !== false) {
              // echo "File is an image - " . $check["mime"] . ".";
              $uploadOk = 1;
          } else {
              echo "File is not an image.";
              $uploadOk = 0;
          }
      }
      // Check if file already exists
      // if (file_exists($target_file)) {
      //     echo "Sorry, file already exists.";
      //     $uploadOk = 0;
      // }
      // Check file size
      // if ($_FILES["fileToUpload"]["size"] > 500000) {
      //     echo "Sorry, your file is too large.";
      //     $uploadOk = 0;
      // }
      // Allow certain file formats
      if($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg"
      && $imageFileType != "gif" ) {
          echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
          $uploadOk = 0;
      }
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
          echo "Sorry, your file was not uploaded.";
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
              // echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";


          } else {
              echo "Sorry, there was an error uploading your file.";
          }
      }
    }
    public function deletepost($post_id)
    {
      $db = Db::getInstance();
      $qry = "DELETE FROM `posts` WHERE `posts`.`id` = :post_id";
      $result = $db->prepare($qry);
      $exec = $result->execute(array(":post_id"=>$post_id));
    }
    public function editpost($post_id,$title,$content,$img,$cetagory){
      $db = Db::getInstance();

      if($img!=''){
        $qry = "UPDATE `posts` SET `title`=:title,`content`=:content,`img`=:img,`cetagory`=:cetagory WHERE id=:id";
        $result = $db->prepare($qry); echo $title;
          $exec = $result->execute(array(
                                        ":title"=>$title,
                                        ":content"=>$content,
                                        ":img"=>$img,
                                        ":cetagory"=>$cetagory,
                                        "id"=>$post_id));
                                      }
      else{
        $qry = "UPDATE `posts` SET `title`=:title,`content`=:content,`cetagory`=:cetagory WHERE id=:id";
        $result = $db->prepare($qry);
        $exec = $result->execute(array(
                                      ":title"=>$title,
                                      ":content"=>$content,
                                      ":cetagory"=>$cetagory,
                                      "id"=>$post_id));
      }
      if($img!=''){
    $target_dir = "img/";
      $target_file = $target_dir . basename($_FILES["fileToUpload"]["name"]);
      $uploadOk = 1;
      $imageFileType = pathinfo($target_file,PATHINFO_EXTENSION);
      // Check if image file is a actual image or fake image
      if(isset($_POST["submit"])) {
          $check = getimagesize($_FILES["fileToUpload"]["tmp_name"]);
          if($check !== false) {
              // echo "File is an image - " . $check["mime"] . ".";
              $uploadOk = 1;
          } else {
              echo "File is not an image.";
              $uploadOk = 0;
          }
      }
      // Check if file already exists
      // if (file_exists($target_file)) {
      //     echo "Sorry, file already exists.";
      //     $uploadOk = 0;
      // }
      // Check file size
      // if ($_FILES["fileToUpload"]["size"] > 500000) {
      //     echo "Sorry, your file is too large.";
      //     $uploadOk = 0;
      // }
      // Allow certain file formats
      if($imageFileType != "jpg" && $imageFileType != "JPG" && $imageFileType != "png" && $imageFileType != "PNG" && $imageFileType != "jpeg"
      && $imageFileType != "gif" ) {
          echo "Sorry, only JPG, JPEG, PNG & GIF files are allowed.";
          $uploadOk = 0;
      }
      // Check if $uploadOk is set to 0 by an error
      if ($uploadOk == 0) {
          echo "Sorry, your file was not uploaded.";
      // if everything is ok, try to upload file
      } else {
          if (move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)) {
              // echo "The file ". basename( $_FILES["fileToUpload"]["name"]). " has been uploaded.";
          } else {
              echo "Sorry, there was an error uploading your file.";
          }
      }
      }
    }




    public function like($post_id,$username)
    {
      $db = Db::getInstance();
      $qry = "INSERT INTO `likes`( `post_id`,`username`) VALUES (:post_id,:username)";
      $result = $db->prepare($qry);
      $exec = $result->execute(array(":post_id"=>$post_id,
                                        ":username"=>$username
                                      ));
    }



    public function dislike($post_id,$username)
    {
        $db = Db::getInstance();
        $qry = "DELETE FROM `likes` WHERE post_id= :post_id AND username= :username";
        $result = $db->prepare($qry);
        $exec = $result->execute(array(":post_id"=>$post_id,
                                          ":username"=>$username
                                        ));
    }
    public static function checklike($post_id,$username)
    {
      $db = Db::getInstance();
      $req = $db->prepare('SELECT * FROM likes WHERE post_id = :post_id AND username=:username');
      $req->execute(array("username"=>$username,
                                  "post_id"=>$post_id));


      if($req->rowCount()){
        return false;
      }else return true;
    }
    public static function checkfollow($following)
    {
      $db = Db::getInstance();
      $req = $db->prepare('SELECT * FROM follow WHERE username = :username AND follower=:follower');
      $req->execute(array("username"=>$following,
                                  "follower"=>$_SESSION['username']));
      if($req->rowCount()){
        return false;
      }else return true;
    }
  }


  class comment
  {
    public $username;
    public $comment;
    public $time;
    function __construct($username,$comment,$time)
    {
      $this->username=$username;
      $this->comment=$comment;
      $this->time=$time;
    }
    public function addcomment($post_id,$username,$comment){
      $db = Db::getInstance();
      $post_id = intval($post_id);
      $qry = "INSERT INTO `comments`( `post_id`,`username`,`comment`) VALUES (:post_id,:username,:comment)";
      $result = $db->prepare($qry);
      $exec = $result->execute(array(":post_id"=>$post_id,
                                        ":username"=>$username,
                                        ":comment"=>$comment));

    }
    public function getcomment($post_id){
      $list = [];
      $db = Db::getInstance();
      $req = $db->query("SELECT * FROM comments WHERE post_id=$post_id ORDER BY id DESC");

      // we create a list of Post objects from the database results
      foreach($req->fetchAll() as $comment) {
        $list[] = new comment($comment['username'],$comment['comment'],$comment['time']);
      }

      return $list;
    }
  }


?>
