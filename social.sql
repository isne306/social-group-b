-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2016 at 06:13 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 7.0.9

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `social`
--

-- --------------------------------------------------------

--
-- Table structure for table `comments`
--

CREATE TABLE `comments` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `comment` text NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `comments`
--

INSERT INTO `comments` (`id`, `post_id`, `username`, `comment`, `time`) VALUES
(1, 9, 'test', 'testcomment', '0000-00-00 00:00:00'),
(2, 9, 'test', 'testcomment', '0000-00-00 00:00:00'),
(3, 9, 'test', 'asd', '0000-00-00 00:00:00'),
(4, 9, 'test', 'qwe', '2016-12-08 21:22:01'),
(5, 9, 'test', 'asd', '2016-12-08 21:23:00'),
(6, 8, 'test', 'testcomment', '2016-12-08 21:24:40'),
(7, 9, 'test2', 'asd', '2016-12-08 21:25:01'),
(8, 10, 'test', 'asd', '2016-12-09 14:56:09'),
(9, 12, 'asd321', 'zxczxczc', '2016-12-13 22:58:13'),
(10, 32, 'asd321', 'asd', '2016-12-14 14:32:15'),
(11, 32, 'asd321', 'asd', '2016-12-14 14:32:16'),
(12, 32, 'asd321', 'asd', '2016-12-14 14:39:01'),
(13, 32, 'asd321', 'asd', '2016-12-14 14:41:37'),
(14, 32, 'asd321', 'asd', '2016-12-14 14:41:46'),
(15, 32, 'asd321', 'asd', '2016-12-14 14:41:59'),
(16, 32, 'asd321', 'asd', '2016-12-14 14:42:09'),
(17, 31, 'asd321', 'aa', '2016-12-14 14:44:16'),
(18, 31, 'asd321', 'aa', '2016-12-14 14:44:45'),
(19, 12, 'asd321', 'asd', '2016-12-14 14:46:02'),
(20, 12, 'asd321', 'asd', '2016-12-14 14:46:23'),
(21, 12, 'asd321', 'asd', '2016-12-14 14:46:26'),
(22, 31, 'Guest', 'asd', '2016-12-15 03:40:47'),
(23, 30, 'Guest', 'asd', '2016-12-15 14:30:28'),
(24, 30, 'Guest', 'asd', '2016-12-15 14:30:56'),
(25, 33, 'cnxisne3019', 'asdasdasd', '2016-12-16 10:28:39'),
(26, 38, 'cnxisne3019', 'à¸ªà¸¸à¸”à¸¢à¸­à¸”à¹„à¸›à¹€à¸¥à¸¢à¸„à¸£à¹Šà¸²à¸šà¸š', '2016-12-16 10:45:28'),
(27, 39, 'cnxisne3019', 'à¸¥à¸­à¸‡à¸­à¸µà¸à¸—à¸µà¸™à¸°', '2016-12-16 11:41:48');

-- --------------------------------------------------------

--
-- Table structure for table `follow`
--

CREATE TABLE `follow` (
  `id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `follower` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `follow`
--

INSERT INTO `follow` (`id`, `username`, `follower`) VALUES
(1, 'test', 'test2'),
(15, 'test2', 'test'),
(17, 'test33', 'test'),
(29, 'test', 'asd321'),
(32, 'asd321', 'test'),
(34, 'asd321', 'cnxisne3019');

-- --------------------------------------------------------

--
-- Table structure for table `likes`
--

CREATE TABLE `likes` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `username` varchar(128) NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `likes`
--

INSERT INTO `likes` (`id`, `post_id`, `username`, `time`) VALUES
(11, 6, 'test2', '2016-12-08 22:27:38'),
(32, 7, 'test2', '2016-12-08 22:33:33'),
(33, 9, 'test2', '2016-12-08 23:00:59'),
(40, 10, 'test', '2016-12-13 18:40:42'),
(42, 9, 'test', '2016-12-13 19:02:53'),
(44, 11, 'test', '2016-12-13 21:04:37'),
(46, 12, 'test2', '2016-12-13 21:07:40'),
(49, 28, 'asd321', '2016-12-13 22:18:52'),
(56, 31, 'asd321', '2016-12-14 14:48:49'),
(125, 8, 'test', '2016-12-15 18:42:59'),
(128, 30, 'test', '2016-12-16 00:29:22'),
(129, 38, 'cnxisne3019', '2016-12-16 10:44:35'),
(130, 39, 'cnxisne3019', '2016-12-16 11:42:31');

-- --------------------------------------------------------

--
-- Table structure for table `login_attempts`
--

CREATE TABLE `login_attempts` (
  `user_id` int(11) NOT NULL,
  `time` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `login_attempts`
--

INSERT INTO `login_attempts` (`user_id`, `time`) VALUES
(1, '1481194212'),
(1, '1481194227'),
(2, '1481204788'),
(2, '1481204817'),
(1, '1481204848'),
(8, '1481220400');

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  `img` varchar(128) NOT NULL DEFAULT 'blank.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `salt`, `img`) VALUES
(4, 'test2', 'test2@test.com', '9e0f5368d9110214aad494704768f6304a9a463fda78d3c06e039d8b11b8d7bc61be38bd05a4d3e8911103e328f43c77e51d9a634e07d8cf275efd7bce81239f', 'a99e53a47da47537de59da6b6829606e8a35438a26d7458243db741c486c67c1afe7e2bb67d6c2933f9eb4530e7be3791601d195570977bb39a2a77147664128', '_MG_0165.JPG'),
(5, 'testipload', 'testasd@test.com', 'f8d6b6f539ef5f37a6c954fbd7da2bc36176e85f9cd9132f6b8eb184a3bb9c1aa0cf4e581a70146acc461b3f3e2e9a097b0d9ab8719ae3584db6883406b68d73', '2622c8915dc31e7a2bc7a9568e734b4476e284137649e45122c35a16259066e422bd1cb8ce19fb5205ea5ef8e9132d846ce771ae2a38e1b08f0d059f67bebc0d', 'blank.png'),
(7, 'asd321qw', 'test2123@test.com', '844db1c8e4ed548ee2dd1eb3515cdeb20addd83de4b23eb75c3527a675ba70af922d7f103e93ff8db7302666e13fcf1d2cf97d1f56f16893a663a4c343673976', 'a69d02cfeea53a8095c5738f9767eb1ac9cdf9a247ce82a80fa37aee4cfe680df273dc2f70f3aa5ddfd7198e53e14d7c334c52227056c7ebfc35a7427db5b2d2', 'blank.png'),
(8, 'asd321', 'test3@test.com', 'a093acfcbec5bb016f5afd26f7c701bf227fe714945edc0854317fc486244869071e067cc739799937bf6ee89b72a2e1d59d83e65a7cf196f86ecfc4ea04137c', 'd07d32eeb670b5c6e8326f49583c5414513575bf3c49111d02f19806966364ca336c6edcaac45d7329abc278012d705c098821619e841b086970271ad03aed6d', '0 (2).jpg'),
(9, 'test', 'test@test.com', '9ffa04636edfd8b52e16864216ce6faeb0b0236ce6fc507f0cf95728156657f6963639da0e89ace06df38266817cdb6d2eac0d6f0bec5046a0a14326cdfb6110', 'f43e88b2bdfd21743851646084e8712f5c6ef99ab1b6444d6fd1973d66ccc94ea8befafa3572c496124ed9288f9b430512c3939d09926bcd995454829f3ee5b3', '_MG_0364.JPG'),
(10, 'test33', 'test33@test.com', '67f855bf32605dab01c9a9193e91f5328ba8a30fd07d7ee587aa53f0f85fe1241509c35f8febaa20b3a625463252f206a7c1b243572544d156779fc35af0fbb9', '870c36db618a29feb94ec66810b5404556c16035b29a36b5d85e75b434c666482be26fbce0f6440833fd79cd7b535e5e2558391115757a127a8ff7d5b7bfb5d6', 'calendar1.PNG'),
(11, 'test333', 'test333@test.com', '7239602b99457670efa7096d8300e238641a4139b51218444ba99428eb941f115802374d708c867a0ab5ff0a884097bc104952ffc16490ba4ace28ecfe556082', '3080de19562b63e1f9fef0aa964cd1318212e90e6fcdba8a93365dc2c2b2d5cd526dd2001ce22c9149f432d6080ded3b376811b4611719022a7b3ccc272fc9b5', 'blank.png'),
(12, 'Guest', '', '', '', 'blank.png');

-- --------------------------------------------------------

--
-- Table structure for table `posts`
--

CREATE TABLE `posts` (
  `id` int(11) NOT NULL,
  `author` text NOT NULL,
  `title` text NOT NULL,
  `content` text NOT NULL,
  `img` text NOT NULL,
  `cetagory` text NOT NULL,
  `time` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `posts`
--

INSERT INTO `posts` (`id`, `author`, `title`, `content`, `img`, `cetagory`, `time`) VALUES
(3, 'asdasd', 'asdasd', 'asdasd', 'asdasd.jpg', 'General', '2016-12-08 19:04:37'),
(4, 'test', 'asdasd', 'asdasd', '_MG_0380.JPG', 'General', '2016-12-08 19:09:03'),
(5, 'test', 'asdasd', 'asdasd', '_MG_0380.JPG', 'General', '2016-12-08 19:09:54'),
(6, 'test', 'test', 'test', '0 (2).jpg', 'General', '2016-12-08 20:40:24'),
(7, 'test', 'test2', 'test2', '_MG_0380.JPG', 'General', '2016-12-08 20:41:46'),
(8, 'test', 'test2', 'qweqweqwe', '_MG_0380.JPG', 'General', '2016-12-08 20:42:43'),
(9, 'test', 'testtitlte', 'testcontent', '115703813.jpg', 'Horror', '2016-12-08 20:51:57'),
(10, 'test2', 'test2', 'test2', '_MG_0373.JPG', 'Horror', '2016-12-08 23:31:30'),
(11, 'test33', 'testpost33', 'test', '_MG_0287.JPG', 'Love', '2016-12-13 17:26:33'),
(12, 'test', 'test1111111111', 'test12313213213213213212', 'calendar1.PNG', 'Sport', '2016-12-13 20:35:43'),
(28, 'asd321', 'sssssssssssssssss', 'asdaaaaaaaaaaaa', 'calendar1.PNG', 'General', '2016-12-13 22:00:17'),
(30, 'asd321', 'asdasdasd', 'qqqqqqqqqqqqqqqqq', '115703813.jpg', 'General', '2016-12-14 00:03:14'),
(32, 'test', 'testnewest', 'ssssssssssssssssssssss', '0.jpg', 'General', '2016-12-15 19:39:37'),
(33, 'test', 'qwe', 'qwe', '2.PNG', 'General', '2016-12-15 20:27:01'),
(34, 'test', 'qwe', 'qweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaa', '', 'General', '2016-12-15 23:32:38'),
(35, 'test', 'qwe', 'qweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaaqweaaaaaaaaaaaaaaaaaaaaaaaaa', '', 'others', '2016-12-15 23:48:04'),
(37, 'cnxisne3019', 'à¸„à¸§à¸²à¸¡à¸œà¸¹à¸à¸žà¸±à¸™', 'à¹€à¸›à¹‡à¸™à¹€à¸£à¸·à¹ˆà¸­à¸‡à¸™à¹ˆà¸²à¸•à¸¥à¸à¹„à¸«à¸¡ à¸—à¸µà¹ˆà¸„à¸™à¹€à¸£à¸²à¸ˆà¸°à¸£à¸¹à¹‰à¸ªà¸¶à¸à¹à¸¥à¸°à¸œà¸¹à¸à¸žà¸±à¸™à¸à¸±à¸šà¸„à¸™à¸­à¸µà¸à¸„à¸™à¸—à¸µà¹ˆà¸­à¸¢à¸¹à¹ˆà¸«à¹ˆà¸²à¸‡à¸à¸±à¸™à¸¡à¸²à¸ à¹€à¸„à¸¢à¹€à¸«à¹‡à¸™à¸«à¸™à¹‰à¸²à¹à¸„à¹ˆà¹ƒà¸™à¹‚à¸—à¸£à¸¨à¸±à¸žà¸—à¹Œ à¹€à¸«à¹‡à¸™à¹€à¸£à¸·à¹ˆà¸­à¸‡à¸£à¸²à¸§à¸—à¸±à¹‰à¸‡à¸«à¸¡à¸”à¹ƒà¸™à¹‚à¸—à¸£à¸¨à¸±à¸žà¸—à¹Œ à¸¡à¸±à¸™à¹€à¸«à¸¡à¸·à¸­à¸™à¹€à¸›à¹‡à¸™à¹€à¸£à¸·à¹ˆà¸­à¸‡à¹€à¸žà¹‰à¸­à¸Ÿà¸±à¸™à¹€à¸™à¸­à¸° à¹à¸•à¹ˆà¸¡à¸±à¸™à¸”à¸±à¸™à¹€à¸à¸´à¸”à¸‚à¸¶à¹‰à¸™à¸à¸±à¸šà¹€à¸£à¸²à¹€à¸ªà¸µà¸¢à¸™à¸µà¹ˆ \r\n     à¹„à¸¡à¹ˆà¸£à¸¹à¹‰à¸§à¹ˆà¸²à¹€à¸¡à¸·à¹ˆà¸­à¹„à¸£à¸—à¸µà¹ˆà¹€à¸£à¸²à¹€à¸œà¸¥à¸­à¸£à¸±à¸à¹€à¸„à¹‰à¸²à¹„à¸›à¹à¸¥à¹‰à¸§ à¹à¸•à¹ˆà¸à¹‡à¸™à¸° à¸£à¸±à¸à¹„à¸›à¹à¸¥à¹‰à¸§à¸ˆà¸°à¹ƒà¸«à¹‰à¹€à¸¥à¸´à¸à¸£à¸±à¸à¸„à¸‡à¸¢à¸²à¸ à¸­à¸µà¸à¸­à¸¢à¹ˆà¸²à¸‡à¸„à¸·à¸­ à¸„à¸§à¸²à¸¡à¸ªà¸¸à¸‚à¸‚à¸­à¸‡à¹€à¸£à¸² à¸—à¸³à¹„à¸¡à¸ˆà¸°à¸•à¹‰à¸­à¸‡à¹€à¸¥à¸´à¸à¸£à¸±à¸à¸”à¹‰à¸§à¸¢à¸¥à¸° à¸„à¸¸à¸“à¸­à¸²à¸ˆà¸ˆà¸°à¸¡à¸­à¸‡à¸à¸±à¸™à¸§à¹ˆà¸² à¸¡à¸±à¸™à¹€à¸›à¹‡à¸™à¹€à¸£à¸·à¹ˆà¸­à¸‡à¹„à¸£à¹‰à¸ªà¸²à¸£à¸° à¸–à¸¶à¸‡à¹à¸Ÿà¸™à¸„à¸¥à¸±à¸šà¸ˆà¸°à¸£à¸±à¸à¸¨à¸´à¸¥à¸›à¸´à¸™à¹à¸¥à¹‰à¸§à¸•à¸±à¸§à¸¨à¸´à¸¥à¸›à¸´à¸™à¸¥à¸° à¸‡à¸±à¹‰à¸™à¹€à¸£à¸²à¸‚à¸­à¸–à¸²à¸¡à¸„à¸¸à¸“à¸šà¹‰à¸²à¸‡ à¸–à¹‰à¸²à¹€à¸›à¹‡à¸™à¸„à¸¸à¸“ à¸žà¸¢à¸²à¸¢à¸²à¸¡à¸—à¸³à¸•à¸²à¸¡à¸„à¸§à¸²à¸¡à¸à¸±à¸™à¸¡à¸²à¸•à¸±à¹‰à¸‡à¹à¸•à¹ˆà¸¢à¸±à¸‡à¹€à¸”à¹‡à¸ à¸ˆà¸™à¹„à¸”à¹‰à¸£à¸±à¸šà¹‚à¸­à¸à¸²à¸ªà¸—à¸µà¹ˆà¸ˆà¸°à¹à¸ªà¸”à¸‡à¸¡à¸±à¸™à¸­à¸­à¸à¸¡à¸² à¹à¸¥à¸°à¸¡à¸µà¸à¸¥à¸¸à¹ˆà¸¡à¸„à¸™à¸„à¸­à¸¢à¸ªà¸™à¸±à¸šà¸ªà¸™à¸¸à¸™ à¸Šà¹ˆà¸§à¸¢à¹€à¸«à¸¥à¸·à¸­ à¸„à¸­à¸¢à¸­à¸¢à¸¹à¹ˆà¸‚à¹‰à¸²à¸‡à¹†à¹€à¸›à¹‡à¸™à¸à¸³à¸¥à¸±à¸‡à¹ƒà¸ˆà¹ƒà¸«à¹‰ à¸„à¸¸à¸“à¸ˆà¸°à¹„à¸¡à¹ˆà¸£à¸¹à¹‰à¸ªà¸¶à¸à¸‚à¸­à¸šà¸„à¸¸à¸“à¸„à¸™à¸à¸¥à¸¸à¹ˆà¸¡à¸™à¸±à¹‰à¸™à¸«à¸™à¹ˆà¸­à¸¢à¹€à¸«à¸£à¸­? à¸–à¸¶à¸‡à¹à¸¡à¹‰à¸ˆà¸°à¹€à¸›à¹‡à¸™à¹à¸Ÿà¸™à¸„à¸¥à¸±à¸š à¹à¸•à¹ˆà¹€à¸£à¸²à¸à¹‡à¸”à¸¹à¸­à¸­à¸ à¸§à¹ˆà¸²à¹€à¸£à¸·à¹ˆà¸­à¸‡à¹„à¸«à¸™à¹€à¸Ÿà¸„à¹„à¸¡à¹ˆà¹€à¸Ÿà¸„ à¹€à¸„à¹‰à¸²à¹„à¸¡à¹ˆà¹„à¸”à¹‰à¸¡à¸²à¹€à¸›à¹‡à¸™à¸¨à¸´à¸¥à¸›à¸´à¸™à¸‡à¹ˆà¸²à¸¢à¹† à¹à¸šà¸šà¹à¸ªà¸à¸™à¸«à¸™à¹‰à¸²à¹€à¸‚à¹‰à¸²à¸¡à¸²à¸–à¹‰à¸²à¸«à¸¥à¹ˆà¸­à¸à¹‡à¹€à¸”à¸šà¸´à¸§à¸•à¹Œà¹„à¸”à¹‰à¹€à¸¥à¸¢ à¸„à¸™à¸šà¸²à¸‡à¸„à¸™à¸‹à¹‰à¸­à¸¡7-8à¸›à¸µ à¸à¸§à¹ˆà¸²à¸ˆà¸°à¸¡à¸µà¹‚à¸­à¸à¸²à¸ªà¹„à¸”à¹‰à¹€à¸”à¸šà¸´à¸§à¸•à¹Œ à¸„à¸™à¸šà¸²à¸‡à¸„à¸™à¸‹à¹‰à¸­à¸¡à¹„à¸¡à¹ˆà¸–à¸¶à¸‡à¸›à¸µà¹à¸•à¹ˆà¸•à¹‰à¸­à¸‡à¸­à¸±à¸”à¸—à¸¸à¸à¸­à¸¢à¹ˆà¸²à¸‡à¹ƒà¸«à¹‰à¸—à¸±à¸™à¸„à¸™à¸­à¸·à¹ˆà¸™à¹€à¸„à¹‰à¸² à¸šà¸²à¸‡à¸„à¸™à¸­à¸­à¸”à¸´à¸Šà¸±à¹ˆà¸™à¹€à¸›à¹‡à¸™à¸ªà¸´à¸šà¸£à¸­à¸š à¸œà¹ˆà¸²à¸™à¸„à¸§à¸²à¸¡à¸œà¸´à¸”à¸«à¸§à¸±à¸‡à¸¡à¸²à¹€à¸›à¹‡à¸™à¸£à¹‰à¸­à¸¢à¸„à¸£à¸±à¹‰à¸‡ à¸žà¸­à¹€à¸„à¹‰à¸²à¹€à¸”à¸šà¸´à¸§à¸•à¹Œ à¸¡à¸±à¸™à¸à¹‡à¹„à¸¡à¹ˆà¹„à¸”à¹‰à¸‡à¹ˆà¸²à¸¢ à¸•à¹‰à¸­à¸‡à¸„à¸­à¸¢à¹‚à¸›à¸£à¹‚à¸¡à¸— à¹à¸¥à¸°à¸—à¸™à¸£à¸±à¸šà¹à¸£à¸‡à¸à¸”à¸”à¸±à¸™à¸ˆà¸²à¸à¸œà¸¹à¹‰à¸„à¸™ à¸à¸§à¹ˆà¸²à¸ˆà¸°à¸‚à¸¶à¹‰à¸™à¸¡à¸²à¸­à¸¢à¸¹à¹ˆà¸šà¸™à¸ˆà¸¸à¸”à¸™à¸µà¹‰à¹„à¸”à¹‰ à¸œà¹ˆà¸²à¸™à¸­à¸°à¹„à¸£à¸¡à¸²à¸¡à¸²à¸à¸¡à¸²à¸¢à¸•à¸±à¹‰à¸‡à¹€à¸¢à¸­à¸° à¹à¸•à¹ˆà¹€à¸„à¹‰à¸²à¸œà¹ˆà¸²à¸™à¸—à¸±à¹‰à¸‡à¸«à¸¡à¸”à¸™à¸±à¹€à¸™à¸¡à¸²à¹„à¸”à¹‰à¸à¸±à¸šà¹€à¹€à¸Ÿà¸™à¸„à¸¥à¸±à¸š à¸„à¸¸à¸“à¸¢à¸±à¸‡à¸ˆà¸°à¸¡à¸­à¸‡à¸§à¹ˆà¸²à¸¡à¸±à¸™à¹„à¸£à¹‰à¸ªà¸²à¸£à¸°à¸­à¸µà¸à¹€à¸«à¸£à¸­?\r\n     à¹€à¸£à¸²à¸¢à¸­à¸¡à¸£à¸±à¸šà¸§à¹ˆà¸²à¸¡à¸µà¹€à¸£à¸·à¹ˆà¸­à¸‡à¸«à¸™à¹‰à¸²à¸•à¸²à¸¡à¸²à¹€à¸à¸µà¹ˆà¸¢à¸§à¸”à¹‰à¸§à¸¢à¸—à¸µà¹ˆà¸£à¸±à¸à¹€à¸„à¹‰à¸² à¹à¸•à¹ˆà¸–à¹‰à¸²à¸Šà¸­à¸šà¹€à¸žà¸£à¸²à¸°à¸«à¸™à¹‰à¸²à¸•à¸²à¸ˆà¸£à¸´à¸‡à¹† à¸—à¸³à¹„à¸¡à¸–à¸¶à¸‡à¸Šà¸­à¸šà¸¡à¸²à¹„à¸”à¹‰à¹€à¸›à¹‡à¸™à¸›à¸µà¹†à¸‚à¸™à¸²à¸”à¸™à¸µà¹‰ à¸—à¸³à¹„à¸¡à¹€à¸£à¸²à¹„à¸¡à¹ˆà¹€à¸šà¸·à¹ˆà¸­ à¸—à¸±à¹‰à¸‡à¹†à¸—à¸µà¹ˆà¸§à¸‡à¸­à¸·à¹ˆà¸™à¸à¹‡à¸«à¸™à¹‰à¸²à¸•à¸²à¸”à¸µà¹€à¸«à¸¡à¸·à¸­à¸™à¸à¸±à¸™ à¹€à¸žà¸£à¸²à¸°à¸—à¸¸à¸à¸„à¸™à¸¡à¸µà¹€à¸­à¸à¸¥à¸±à¸à¸©à¸“à¹Œà¸›à¸£à¸°à¸ˆà¸³à¸•à¸±à¸§à¹„à¸‡ à¸—à¸±à¹‰à¸‡à¸™à¸´à¸ªà¸±à¸¢à¸—à¸µà¹ˆà¹à¸ªà¸”à¸‡à¸­à¸­à¸à¸¡à¸²à¹à¸šà¸šà¸‹à¸·à¹ˆà¸­à¹†à¸‹à¸™à¹† à¹€à¸«à¸¡à¸·à¸­à¸™à¹€à¸”à¹‡à¸à¹€à¸§à¸¥à¸²à¸­à¸¢à¸¹à¹ˆà¸à¸±à¸šà¹€à¸žà¸·à¹ˆà¸­à¸™ à¸ˆà¸‡à¹à¸”à¸ˆà¸°à¸‚à¸µà¹‰à¹‚à¸§à¸¢à¸§à¸²à¸¢ à¸Šà¸­à¸šà¸‡à¸­à¹à¸‡ à¹€à¸à¸£à¸µà¸¢à¸™à¹†à¹à¸•à¹ˆà¸à¹‡à¹€à¸›à¹‡à¸™à¸„à¸™à¸—à¸µà¹ˆà¹€à¸­à¸²à¹ƒà¸ˆà¹ƒà¸ªà¹ˆà¹à¸¥à¸°à¸­à¸šà¸­à¸¸à¹ˆà¸™ à¹€à¸žà¸£à¸²à¸°à¹à¸šà¸šà¸™à¸±à¹‰à¸™à¹€à¸£à¸²à¸–à¸¶à¸‡à¸£à¸±à¸à¹€à¸„à¹‰à¸² à¹à¸¥à¸°à¸ªà¸´à¹ˆà¸‡à¸—à¸µà¹ˆà¸ªà¸³à¸„à¸±à¸à¸—à¸µà¹ˆà¸ªà¸¸à¸”à¸„à¸·à¸­à¹€à¸ªà¸µà¸¢à¸‡à¸£à¹‰à¸­à¸‡à¹€à¸žà¸¥à¸‡à¸‚à¸­à¸‡à¹€à¸„à¹‰à¸² à¸¡à¸±à¸™à¸ªà¸°à¸à¸”à¹ƒà¸ˆà¹€à¸£à¸²à¹„à¸§à¹‰à¸—à¸¸à¸à¸„à¸£à¸±à¹‰à¸‡à¸—à¸µà¹ˆà¸Ÿà¸±à¸‡ à¸–à¹‰à¸²à¹„à¸¡à¹ˆà¸£à¸¹à¹‰à¸§à¹ˆà¸²à¹€à¸žà¸£à¸²à¸°à¸‚à¸™à¸²à¸”à¹„à¸«à¸™à¸«à¸¥à¸±à¸à¸à¸²à¸™à¸à¹‡à¹€à¸žà¸¥à¸‡ everytime à¸—à¸µà¹ˆà¸”à¸±à¸‡à¹†à¸™à¸±à¹ˆà¸™à¹„à¸‡ à¸›à¸£à¸°à¸à¸­à¸šà¸‹à¸µà¸£à¸µà¹ˆà¸¢à¹Œà¸à¸±à¸›à¸•à¸±à¸™à¸à¸±à¸šà¸«à¸¡à¸­à¸™à¹ˆà¸° à¸™à¸±à¹‰à¸™à¹à¸«à¸¥à¸° à¹€à¸žà¸£à¸²à¸°à¸¡à¸²à¸à¸à¸à¸à¸ \r\n     à¹€à¸£à¸²à¹„à¸¡à¹ˆà¸ˆà¸³à¹€à¸›à¹‡à¸™à¸•à¹‰à¸­à¸‡à¸«à¸²à¹€à¸«à¸•à¸¸à¸œà¸¥à¸¡à¸²à¸£à¸­à¸‡à¸£à¸±à¸šà¸«à¸£à¸­à¸à¸§à¹ˆà¸²à¸—à¸³à¹„à¸¡à¸–à¸¶à¸‡à¸£à¸±à¸ à¸£à¸±à¸à¸à¹‡à¸„à¸·à¸­à¸£à¸±à¸ à¹„à¸¡à¹ˆà¸ˆà¸³à¹€à¸›à¹‡à¸™à¸•à¹‰à¸­à¸‡à¸žà¸¹à¸”à¸­à¸°à¹„à¸£à¸¡à¸²à¸ à¹€à¸£à¸²à¸£à¸¹à¹‰à¸­à¸¢à¸¹à¹ˆà¹à¸à¹ˆà¹ƒà¸ˆà¸§à¹ˆà¸²à¹€à¸£à¸²à¸£à¸¹à¹‰à¸ªà¸¶à¸à¸¢à¸±à¸‡à¹„à¸‡ à¸‚à¸­à¸šà¸„à¸¸à¸“à¸™à¸°à¸—à¸µà¹ˆà¹€à¸‚à¹‰à¸²à¸¡à¸²à¸ªà¸£à¹‰à¸²à¸‡à¸ªà¸µà¸ªà¸±à¸™à¹ƒà¸«à¹‰à¸à¸±à¸šà¸Šà¸µà¸§à¸´à¸•à¸Šà¹ˆà¸§à¸‡à¸«à¸™à¸¶à¹ˆà¸‡à¸‚à¸­à¸‡à¹€à¸£à¸² à¸¡à¸²à¸­à¸¢à¸¹à¹ˆà¸”à¹‰à¸§à¸¢à¸à¸±à¸™à¹„à¸›à¸ˆà¸™à¸ªà¸¸à¸”à¸—à¸²à¸‡à¹€à¸¥à¸¢à¸™à¸°', '14817599796256867252.jpg', 'General', '2016-12-16 10:42:53'),
(38, 'cnxisne3019', 'à¹€à¸›à¹‡à¸™à¸”à¸±à¹ˆà¸‡à¸£à¹ˆà¸¡à¹€à¸‡à¸²', 'à¸•à¹‰à¸™à¹„à¸¡à¹‰à¸—à¸µà¹ˆà¹€à¸›à¹‡à¸™à¸”à¸±à¹ˆà¸‡à¸£à¹ˆà¸¡à¹€à¸‡à¸²......\r\n\r\n\r\n\r\nà¸šà¸²à¸‡à¸„à¸£à¸±à¹‰à¸‡à¸šà¸²à¸‡à¸„à¸£à¸²à¸§à¸„à¸§à¸²à¸¡à¸£à¹ˆà¸¡à¹€à¸‡à¸²à¸™à¸±à¹‰à¸™\r\nà¹€à¸›à¹‡à¸™à¹„à¸”à¹‰à¹à¸„à¹ˆà¸—à¸µà¹ˆà¸žà¸±à¸à¸žà¸´à¸‡à¹ƒà¸«à¹‰à¸à¸±à¸šà¹€à¸«à¸¥à¹ˆà¸²à¸™à¸à¸™à¹‰à¸­à¸¢à¸«à¸¥à¸²à¸à¸«à¸¥à¸²à¸¢à¸Šà¸™à¸´à¸”à¸—à¸µà¹ˆà¸¡à¸µà¸à¸²à¸£à¸¡à¸²à¸”à¹‰à¸§à¸¢à¸à¸²à¸‡à¸›à¸µà¸à¸„à¹ˆà¸­à¸¢à¹†à¸„à¸¥à¹‰à¸­à¸¢à¹‚à¸šà¸¢à¸šà¸´à¸™à¸œà¹ˆà¸²à¸™à¸¡à¸²à¹à¸¥à¹‰à¸§à¸¥à¸‡à¸¡à¸²à¹€à¸à¸²à¸°à¸•à¸²à¸¡à¸‹à¸­à¸à¸¡à¸¸à¸¡à¹†à¸«à¸™à¸¶à¹ˆà¸‡à¸‚à¸­à¸‡à¸•à¹‰à¸™à¹„à¸¡à¹‰ à¸­à¸µà¸à¸ªà¸±à¸à¸„à¸£à¸¹à¹ˆà¸šà¸£à¸£à¸”à¸²à¹€à¸«à¸¥à¹ˆà¸²à¸™à¸à¸™à¹‰à¸­à¸¢à¸à¹‡à¸„à¹ˆà¸­à¸¢à¹†à¸ªà¸¹à¸”à¸”à¸¡à¸à¸¥à¸´à¹ˆà¸™à¹à¸«à¹ˆà¸‡à¸„à¸§à¸²à¸¡à¸£à¹ˆà¸¡à¹€à¸¢à¹‡à¸™à¸—à¸µà¹ˆà¸Šà¸·à¹ˆà¸™à¸Šà¸¥à¹ƒà¸ˆà¸ˆà¸™à¹ƒà¸™à¸—à¸µà¹ˆà¸ªà¸¸à¸”à¸„à¸§à¸²à¸¡à¸£à¹ˆà¸¡à¹€à¸¢à¹‡à¸™à¸™à¸µà¹‰à¸ªà¸²à¸¡à¸²à¸£à¸–à¸”à¸±à¸šà¸„à¸§à¸²à¸¡à¸£à¹‰à¸­à¸™à¸£à¸™à¹ƒà¸™à¹ƒà¸ˆà¸—à¸µà¹ˆà¸¡à¸µà¸™à¸±à¹‰à¸™à¸¡à¸²à¸à¹ˆà¸­à¸™à¸«à¸™à¹‰à¸²à¸™à¸µà¹‰ \r\n\r\n\r\nà¹€à¸¡à¸·à¹ˆà¸­à¸™à¸±à¸à¹€à¸«à¸¥à¹ˆà¸²à¸šà¸£à¸£à¸”à¸²à¸™à¸à¸™à¹‰à¸­à¸¢à¸šà¸²à¸‡à¸ªà¹ˆà¸§à¸™à¹„à¸”à¹‰à¸£à¸±à¸šà¸¥à¸´à¹‰à¸¡à¸£à¸ªà¸žà¸¥à¸±à¸‡à¸‡à¸²à¸™à¸„à¸§à¸²à¸¡à¸£à¹ˆà¸¡à¹€à¸¢à¹‡à¸™à¸­à¸¢à¹ˆà¸²à¸‡à¸­à¸´à¹ˆà¸¡à¹€à¸­à¸¡à¹ƒà¸ˆà¹à¸¥à¸°à¹€à¸¡à¸·à¹ˆà¸´à¸´à¸­à¹„à¸”à¹‰à¹€à¸§à¸¥à¸²à¸—à¸µà¹ˆà¸ˆà¸°à¸•à¹‰à¸­à¸‡à¹„à¸›à¹€à¸«à¸¥à¹ˆà¸²à¸šà¸£à¸£à¸”à¸²à¸™à¸à¸™à¹‰à¸­à¸¢à¹€à¸à¸·à¸­à¸šà¸—à¸¸à¸à¸•à¸±à¸§à¸à¹‡à¹€à¸žà¸µà¸¢à¸šà¸žà¸£à¹‰à¸­à¸¡à¹ƒà¸ˆà¸—à¸µà¹ˆà¸ˆà¸°à¸—à¹ˆà¸­à¸‡à¹€à¸—à¸µà¹ˆà¸¢à¸§à¹‚à¸šà¸¢à¸šà¸´à¸™à¹„à¸›à¸–à¸¶à¸‡à¸­à¸·à¹ˆà¸™à¸•à¹ˆà¸­à¹„à¸›... \r\n\r\n\r\nà¹à¸¥à¸°à¸•à¹ˆà¸­à¹„à¸›... à¸—à¸µà¹ˆà¹„à¸¡à¹ˆà¸¡à¸µà¸§à¸±à¸™à¸£à¸¹à¹‰à¹„à¸”à¹‰à¹€à¸¥à¸¢à¸§à¹ˆà¸²à¸ˆà¸°à¸à¸¥à¸±à¸šà¸¡à¸²à¸™à¸±à¸à¸•à¹‰à¸™à¹„à¸¡à¹‰à¸£à¹ˆà¸¡à¹€à¸‡à¸²à¸™à¸µà¹ˆà¹„à¸«à¸¡!\r\n\r\n\r\nà¹à¸•à¹ˆà¸à¹‡à¸¢à¸±à¸‡à¸„à¸‡à¸«à¸¥à¸‡à¹€à¸«à¸¥à¸·à¸­à¹€à¸«à¸¥à¹ˆà¸²à¸šà¸£à¸£à¸”à¸²à¸™à¸à¸™à¹‰à¸­à¸¢à¸—à¸µà¹ˆà¸¡à¸µà¸ˆà¸³à¸™à¹‰à¸­à¸¢à¸™à¸´à¸”à¸™à¸±à¹‰à¸™ à¸—à¸µà¹ˆà¸•à¸±à¸”à¸ªà¸´à¸™à¹ƒà¸ˆà¹„à¸¡à¹ˆà¹„à¸›à¹€à¸—à¸µà¹ˆà¸¢à¸§à¹€à¸•à¸£à¹ˆà¹„à¸›à¹„à¸à¸¥à¸—à¸µà¹ˆà¹„à¸«à¸™ à¹à¸•à¹ˆà¸—à¸µà¹ˆà¸™à¹ˆà¸²à¸›à¸£à¸°à¸«à¸¥à¸²à¸”à¹ƒà¸ˆ à¸à¹‡à¸„à¸·à¸­à¹€à¸«à¸¥à¹ˆà¸²à¸šà¸£à¸£à¸”à¸²à¸™à¸à¸™à¸±à¹ˆà¸™ à¸à¸¥à¸±à¸šà¸¡à¸µà¸„à¸§à¸²à¸¡à¸•à¹‰à¸­à¸‡à¸à¸²à¸£à¸—à¸µà¹ˆà¸ˆà¸°à¸­à¸²à¸¨à¸±à¸¢à¸­à¸¢à¸¹à¹ˆà¸£à¹ˆà¸§à¸¡à¸à¸±à¸šà¸•à¹‰à¸™à¹„à¸¡à¹‰à¸œà¸¹à¹‰à¹ƒà¸ˆà¹ƒà¸«à¸à¹ˆà¸•à¹‰à¸™à¸™à¸µà¹‰\r\n\r\n\r\nà¸–à¸¶à¸‡à¹à¸¡à¹‰à¸§à¹ˆà¸²à¸™à¸à¸™à¹‰à¸­à¸¢à¸«à¸¡à¸¹à¹ˆà¹€à¸«à¸¥à¹ˆà¸²à¸™à¸µà¹‰à¹„à¸¡à¹ˆà¹„à¸”à¹‰à¸—à¸±à¸à¸–à¸²à¸¡à¸‚à¸­à¸­à¸™à¸¸à¸à¸²à¸•à¸•à¹ˆà¸­à¸•à¹‰à¸™à¹„à¸¡à¹‰à¸•à¹‰à¸™à¸™à¸µà¹‰à¸à¹ˆà¸­à¸™ à¸ˆà¸°à¸à¹ˆà¸­à¸ªà¸£à¹‰à¸²à¸‡à¸£à¸±à¸‡à¸‚à¸­à¸‡à¸•à¸±à¸§à¹€à¸­à¸‡ à¹à¸•à¹ˆà¸•à¹‰à¸™à¹„à¸¡à¹‰à¸•à¹‰à¸™à¸™à¸µà¹‰à¸à¹‡à¸ˆà¸°à¸¡à¸µà¹€à¸žà¸µà¸¢à¸‡à¹à¸„à¹ˆà¸„à¸³à¸•à¸­à¸šà¹€à¸”à¸µà¸¢à¸§à¹€à¸—à¹ˆà¸²à¸™à¸±à¹‰à¸™à¸—à¸µà¹ˆà¸ˆà¸°à¸•à¸­à¸šà¸à¸¥à¸±à¸šà¹„à¸›à¸§à¹ˆà¸²  à¸„à¸³à¸•à¸­à¸šà¸™à¸±à¹ˆà¸™à¸à¹‡à¸„à¸·à¸­ "à¹€à¸­à¸²à¸—à¸µà¹ˆà¸ªà¸šà¸²à¸¢à¹ƒà¸ˆà¹€à¸¥à¸¢à¸™à¹ˆà¸°" à¹à¸•à¹ˆà¸‚à¸­à¹ƒà¸«à¹‰à¸­à¸¢à¸¹à¹ˆà¸­à¸¢à¹ˆà¸²à¸‡à¸ªà¸‡à¸šà¸ªà¸¸à¸‚à¸•à¹ˆà¸­à¸à¸±à¸™à¸à¹‡à¹€à¸žà¸µà¸¢à¸‡à¸žà¸­à¹à¸¥à¹‰à¸§ à¸•à¹‰à¸™à¹„à¸¡à¹‰à¸•à¹‰à¸™à¸™à¸µà¹‰à¸‚à¸­à¹€à¸žà¸µà¸¢à¸‡à¹€à¸—à¹ˆà¸²à¸™à¸µà¹‰à¸ˆà¸£à¸´à¸‡à¹†\r\n\r\n\r\nà¹€à¸¡à¸·à¹ˆà¸­à¹€à¸«à¸¥à¹ˆà¸²à¸šà¸£à¸£à¸”à¸²à¸™à¸à¸Ÿà¸±à¸‡à¸„à¸³à¸•à¸­à¸šà¸™à¸±à¹ˆà¸™à¹à¸¥à¹‰à¸§\r\nà¸•à¹ˆà¸²à¸‡à¸à¹‡à¸”à¸µà¸­à¸à¸”à¸µà¹ƒà¸ˆà¸ˆà¸™à¹à¸ªà¸”à¸‡à¸—à¹ˆà¸²à¸—à¸µà¸­à¸­à¸à¸¡à¸²à¸”à¹‰à¸§à¸¢à¸à¸²à¸£à¸à¸£à¸°à¹‚à¸”à¸”à¹‚à¸¥à¸”à¹€à¸•à¹‰à¸™à¹à¸¥à¸°à¸ªà¹ˆà¸‡à¹€à¸ªà¸µà¸¢à¸‡à¸£à¹‰à¸­à¸‡à¸­à¸¢à¹ˆà¸²à¸‡à¹„à¸žà¹€à¸£à¸²à¸°à¹€à¸ªà¸™à¸²à¸°à¸«à¸¹à¸­à¸­à¸à¸¡à¸²à¹‚à¸”à¸¢à¹„à¸¡à¹ˆà¸£à¸¹à¹‰à¸•à¸±à¸§à¹€à¸¥à¸¢\r\n\r\n\r\nà¸¡à¸µà¸™à¸à¸•à¸±à¸§à¸«à¸™à¸¶à¹ˆà¸‡à¹„à¸”à¹‰à¸šà¸­à¸à¸§à¹ˆà¸²...\r\nà¸™à¸µà¹ˆà¹†à¹† à¸žà¸§à¸à¹€à¸˜à¸­à¹€à¸«à¹‡à¸™à¹„à¸«à¸¡!! \r\nà¸•à¹‰à¸™à¹„à¸¡à¹‰à¸—à¸µà¹ˆà¹à¸ªà¸™à¸£à¹ˆà¸¡à¹€à¸‡à¸²à¸•à¹‰à¸™à¸™à¸µà¹‰à¸—à¸µà¹ˆà¸”à¸§à¸‡à¹ƒà¸ˆà¹€à¸›à¸£à¸µà¸¢à¸šà¹€à¸ªà¸¡à¸·à¸­à¸™à¸”à¸±à¹ˆà¸‡à¹à¸¡à¹ˆà¸™à¹‰à¸³à¸¡à¸«à¸²à¸ªà¸¡à¸¸à¸—à¸£à¸à¸§à¹‰à¸²à¸‡à¹ƒà¸«à¸à¹ˆà¸™à¸±à¹‰à¸™\r\n\r\n\r\nà¸šà¸£à¸£à¸”à¸²à¸™à¸à¸•à¸±à¸§à¸­à¸·à¹ˆà¸™à¸à¹‡à¹€à¸«à¹‡à¸™à¸”à¹‰à¸§à¸¢à¸•à¸£à¸‡à¸à¸±à¸™à¸§à¹ˆà¸²\r\nà¸•à¹‰à¸™à¹„à¸¡à¹‰à¸”à¸±à¹ˆà¸‡à¸£à¹ˆà¸¡à¹€à¸‡à¸²à¸™à¸µà¹‰à¸Šà¸±à¹ˆà¸‡à¹ƒà¸ˆà¹à¸ªà¸™à¸”à¸µà¸™à¸±à¸\r\n\r\n\r\nà¹€à¸¡à¸·à¹ˆà¸­à¸¤à¸”à¸¹à¸à¸™à¸—à¸±à¸à¹€à¸‚à¹‰à¸²à¸¡à¸²à¸«à¸² à¸§à¸±à¸™à¸™à¸±à¹‰à¸™à¸à¸™à¸à¹‡à¹€à¸£à¸´à¹ˆà¸¡à¸•à¸à¸žà¸£à¹ˆà¸³à¸«à¸¥à¹ˆà¸™à¸¡à¸²à¹€à¸›à¹‡à¸™à¸”à¸±à¹ˆà¸‡à¸ªà¸²à¸¢ à¹„à¸¡à¹ˆà¸£à¸¹à¹‰à¸ˆà¸±à¸à¸«à¸¢à¸¸à¸”à¸«à¸¢à¹ˆà¸­à¸™à¸ªà¸±à¸à¹€à¸ªà¸µà¸¢à¸—à¸µ....\r\n\r\nà¸šà¸£à¸£à¸”à¸²à¸™à¸à¹€à¸«à¸¥à¹ˆà¸²à¸™à¸±à¹‰à¸™à¸à¹‡à¸¢à¸±à¸‡à¸„à¸‡à¸«à¸¥à¸šà¸­à¸¢à¸¹à¹ˆà¹ƒà¸™à¸£à¸¸à¸‡à¸£à¸±à¸‡à¸™à¸±à¹‰à¸™à¹€à¸«à¸¡à¸·à¸­à¸™à¸”à¸±à¹ˆà¸‡à¹€à¸„à¸¢....\r\n\r\nà¸ªà¹ˆà¸§à¸™à¸•à¹‰à¸™à¹„à¸¡à¹‰à¸•à¹‰à¸™à¸™à¸µà¹‰à¸à¸¥à¸±à¸šà¹€à¸«à¸™à¹‡à¸šà¸«à¸™à¸²à¸§à¸ˆà¸±à¸šà¹ƒà¸ˆà¸™à¸±à¸ à¹à¸¥à¸°à¸žà¸£à¹‰à¸­à¸¡à¸­à¸˜à¸´à¸à¸²à¸™à¹€à¸‡à¸µà¸¢à¸šà¸‡à¸±à¸™à¸§à¹ˆà¸².....\r\n\r\n\r\n"à¸‚à¸­à¹ƒà¸«à¹‰à¸žà¸£à¸¸à¹ˆà¸‡à¸™à¸µà¹‰à¹à¸ªà¸‡à¸­à¸²à¸—à¸´à¸•à¸¢à¹Œà¹à¸ªà¸”à¸‡à¸•à¸±à¸§à¸­à¸­à¸à¸¡à¸²à¸ªà¸­à¸”à¸ªà¹ˆà¸­à¸‡à¹à¸ªà¸‡à¸ªà¸§à¹ˆà¸²à¸‡à¸­à¸¸à¹ˆà¸™à¹†à¸™à¸±à¹ˆà¸™à¸­à¸­à¸à¸¡à¸²à¹ƒà¸«à¹‰à¹„à¸”à¹‰à¸ªà¸±à¸¡à¸œà¸±à¸ªà¹€à¸ªà¸µà¸¢à¸—à¸µ"\r\n\r\n\r\nà¹à¸¥à¸°à¸„à¸³à¸­à¸˜à¸´à¸à¸²à¸™à¹€à¸«à¸¥à¹ˆà¸²à¸™à¸±à¹‰à¸™à¸à¹‡à¹€à¸›à¹‡à¸™à¸”à¸±à¹ˆà¸‡à¸„à¸§à¸²à¸¡à¸ˆà¸£à¸´à¸‡\r\nà¸§à¸±à¸™à¸§à¸²à¸™ à¸—à¸µà¹ˆà¸šà¸­à¸à¸§à¹ˆà¸² à¹€à¸›à¹‡à¸™à¸§à¸±à¸™à¸žà¸£à¸¸à¹ˆà¸‡à¸™à¸µà¹‰ à¹€à¸¡à¸·à¹ˆà¸­à¸§à¸±à¸™à¸žà¸£à¸¸à¹ˆà¸‡à¸™à¸µà¹‰à¸¡à¸²à¸–à¸¶à¸‡à¸à¹‡à¸à¸¥à¸±à¸šà¸à¸¥à¸²à¸¢ à¹€à¸›à¹‡à¸™à¸§à¸±à¸™à¸™à¸µà¹‰\r\n', '14817248405598108000.jpg', 'General', '2016-12-16 10:43:37'),
(39, 'cnxisne3019', 'à¸à¸µà¸¬à¸²à¸ªà¸µà¹€à¹€à¸¢à¹‰à¸§à¸§à¸§à¸§', 'à¹‚à¸£à¸‡à¹€à¸£à¸µà¸¢à¸™à¹€à¸£à¸²à¸¡à¸µà¸‡à¸²à¸™à¸à¸µà¸¬à¸²à¸ªà¸µà¹€à¹€à¸¥à¹‰à¸§à¸§à¸§ \r\nà¹€à¸­à¹‹!!à¸•à¸­à¸™à¹€à¹€à¸£à¸à¸¥à¸¸à¹‰à¸™à¸¡à¸²à¸à¸§à¹ˆà¸²à¸ˆà¸°à¹„à¸”à¹‰à¸­à¸¢à¸¹à¹ˆà¸ªà¸µà¹€à¸”à¸µà¸¢à¸§à¸à¸±à¸šà¸„à¸™à¸—à¸µà¹ˆà¸Šà¸­à¸šà¸«à¸£à¸·à¸­à¸›à¹ˆà¸²à¸§à¸§\r\nà¹€à¸¢à¹‰à¹†à¹†ðŸ˜€ðŸ˜€\r\nà¹„à¸”à¹‰à¸­à¸¢à¸¹à¹ˆà¸ªà¸µà¹€à¸”à¸µà¸¢à¸§à¸à¸±à¸™à¸™à¸™à¸™ à¸–à¹‰à¸²à¹€à¸‚à¸²à¹€à¹€à¸‚à¹ˆà¸‡à¸šà¸­à¸¥à¹€à¸£à¸²à¸ˆà¸°à¹„à¸›à¹€à¸Šà¸µà¸¢à¸£à¹Œà¸™à¹ˆà¸°55555\r\nà¹€à¹€à¸¥à¸°à¸à¹‡à¸–à¸¶à¸‡à¸§à¸±à¸™à¹€à¹€à¸‚à¹ˆà¸‡à¹€à¸‚à¸²à¸¥à¸‡à¸ªà¸™à¸²à¸¡ à¹‚à¸­à¹‰à¸¢à¸¢à¸¢à¹€à¸˜à¸­à¸­à¸­à¸­ à¸‚à¸²à¸§à¸—à¸µà¹ˆà¸ªà¸¸à¸”à¹ƒà¸™à¸ªà¸™à¸²à¸¡à¹€à¸¥à¸¢à¸¢à¸¢ à¸£à¸­à¸‡à¹€à¸—à¹‰à¸²à¸ªà¸µà¸šà¸²à¸”à¹ƒà¸ˆà¸¡à¸²à¸555 à¸ªà¸µà¸ªà¹‰à¸¡à¹€à¹€à¸ªà¸”555\r\nà¹€à¸¥à¹ˆà¸™à¹€à¸à¹ˆà¸‡à¸™à¹ˆà¸°à¹€à¸™à¸µà¹ˆà¸¢ à¹‚à¸«à¸¢à¸¢à¹† à¸„à¸™à¸‚à¸­à¸‡à¹ƒà¸ˆà¹ƒà¸„à¸£à¹€à¸™à¸µà¹ˆà¸¢à¸¢555 à¸™à¹ˆà¸²à¸£à¸±à¸à¸Šà¸°à¸¡à¸±à¸”à¸” \r\nà¹€à¸«à¹‰à¸¢à¸¢à¸¢ à¸¥à¸·à¹ˆà¸™ à¸¥à¸‡à¹„à¸›à¸™à¸­à¸™à¸—à¸³à¹„à¸¡5555 \r\nà¹„à¸¡à¹ˆà¹€à¸›à¹‡à¸™à¹„à¸£à¸™à¹ˆà¸°à¹„à¸¡à¹ˆà¹€à¸›à¹‡à¸™à¹„à¸£ 55555\r\nà¸žà¸­à¹€à¹€à¸‚à¹ˆà¸‡à¹€à¸ªà¸£à¹‡à¸ˆ  à¹€à¸‚à¸²à¸­à¸­à¸à¸ˆà¸²à¸à¸ªà¸™à¸²à¸¡à¸¡à¸²à¸à¸´à¸™à¸™à¹‰à¸³ à¹€à¸£à¸²à¸à¹‡à¸¥à¸‡à¹„à¸›555 à¸„à¸£à¸²à¸§à¸™à¸µà¹‰à¹„à¸”à¹‰à¹ƒà¸à¸¥à¹‰à¸à¸±à¸™à¸”à¹‰à¸§à¸¢à¸¢à¸¢ à¸—à¸³à¹„à¸¡à¸«à¸™à¹‰à¸²à¹€à¹€à¸”à¸‡à¸ˆà¸±à¸‡ à¸ªà¸‡à¸ªà¸±à¸¢à¸„à¸‡à¹€à¸«à¸™à¸·à¹ˆà¸­à¸¢à¸¡à¸²à¸à¸ªà¸´à¹‰à¸™à¹ˆà¸° à¹€à¹€à¸¥à¹‰à¸§à¸à¹‡à¸¡à¸µà¸£à¸¸à¹ˆà¸™à¸žà¸µà¹ˆà¸„à¸™à¸™à¸¶à¸‡à¹€à¸›à¹‡à¸™à¹€à¹€à¸¡à¹ˆà¸ªà¸µà¹€à¸£à¸² à¹€à¸‚à¸²à¸Šà¸­à¸šà¸„à¸™à¹€à¸”à¸µà¸¢à¸§à¸à¸±à¸šà¹€à¸£à¸²à¹€à¸‚à¸²à¸à¹‡à¸¢à¸·à¸™à¸žà¸±à¸”à¹ƒà¸«à¹‰à¸„à¸™à¸—à¸µà¹ˆà¹€à¸£à¸²à¸Šà¸­à¸š à¸§à¹‰à¸²à¸¢à¹†à¹€à¸‚à¸²à¹€à¸”à¸´à¸™à¸«à¸™à¸µà¸­à¹ˆà¸°55555', '14728244165012173097.jpg', 'Sport', '2016-12-16 10:47:47');

-- --------------------------------------------------------

--
-- Table structure for table `readed`
--

CREATE TABLE `readed` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `user` varchar(128) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `readed`
--

INSERT INTO `readed` (`id`, `post_id`, `user`) VALUES
(1, 30, '8'),
(2, 30, '8'),
(3, 28, '8'),
(4, 7, '8'),
(5, 6, '8'),
(6, 10, '8'),
(7, 12, '8'),
(8, 9, '8'),
(9, 8, '8'),
(10, 5, '8'),
(11, 4, '8'),
(12, 31, '8'),
(13, 30, '9'),
(14, 28, '9'),
(15, 11, '9'),
(16, 10, '9'),
(17, 32, '8'),
(18, 31, '9'),
(19, 7, '9'),
(20, 32, '4'),
(21, 12, '4'),
(22, 9, '4'),
(23, 32, '9'),
(24, 33, '9'),
(25, 5, '9'),
(26, 34, '9'),
(27, 35, '9'),
(28, 35, '11'),
(29, 36, '11'),
(30, 33, '11'),
(31, 38, '11'),
(32, 4, '11'),
(33, 39, '11'),
(34, 37, '11');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` bigint(20) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `avatar` text NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=utf8;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `username`, `password`, `email`, `avatar`) VALUES
(1, 'test', '123qweASD', 'test@test.com', '123qweASD'),
(2, 'test2', '123qweASD', 'test2@test.com', 'qwe');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `comments`
--
ALTER TABLE `comments`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `follow`
--
ALTER TABLE `follow`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `likes`
--
ALTER TABLE `likes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `posts`
--
ALTER TABLE `posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `readed`
--
ALTER TABLE `readed`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `comments`
--
ALTER TABLE `comments`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `follow`
--
ALTER TABLE `follow`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `likes`
--
ALTER TABLE `likes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=131;
--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
--
-- AUTO_INCREMENT for table `posts`
--
ALTER TABLE `posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=40;
--
-- AUTO_INCREMENT for table `readed`
--
ALTER TABLE `readed`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
