-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Dec 16, 2016 at 06:13 AM
-- Server version: 10.1.16-MariaDB
-- PHP Version: 5.6.24

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `social`
--

-- --------------------------------------------------------

--
-- Table structure for table `members`
--

CREATE TABLE `members` (
  `id` int(11) NOT NULL,
  `username` varchar(30) NOT NULL,
  `email` varchar(30) NOT NULL,
  `password` char(128) NOT NULL,
  `salt` char(128) NOT NULL,
  `img` varchar(128) NOT NULL DEFAULT 'blank.png'
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data for table `members`
--

INSERT INTO `members` (`id`, `username`, `email`, `password`, `salt`, `img`) VALUES
(4, 'test2', 'test2@test.com', '9e0f5368d9110214aad494704768f6304a9a463fda78d3c06e039d8b11b8d7bc61be38bd05a4d3e8911103e328f43c77e51d9a634e07d8cf275efd7bce81239f', 'a99e53a47da47537de59da6b6829606e8a35438a26d7458243db741c486c67c1afe7e2bb67d6c2933f9eb4530e7be3791601d195570977bb39a2a77147664128', '_MG_0165.JPG'),
(5, 'testipload', 'testasd@test.com', 'f8d6b6f539ef5f37a6c954fbd7da2bc36176e85f9cd9132f6b8eb184a3bb9c1aa0cf4e581a70146acc461b3f3e2e9a097b0d9ab8719ae3584db6883406b68d73', '2622c8915dc31e7a2bc7a9568e734b4476e284137649e45122c35a16259066e422bd1cb8ce19fb5205ea5ef8e9132d846ce771ae2a38e1b08f0d059f67bebc0d', 'blank.png'),
(7, 'asd321qw', 'test2123@test.com', '844db1c8e4ed548ee2dd1eb3515cdeb20addd83de4b23eb75c3527a675ba70af922d7f103e93ff8db7302666e13fcf1d2cf97d1f56f16893a663a4c343673976', 'a69d02cfeea53a8095c5738f9767eb1ac9cdf9a247ce82a80fa37aee4cfe680df273dc2f70f3aa5ddfd7198e53e14d7c334c52227056c7ebfc35a7427db5b2d2', 'blank.png'),
(8, 'asd321', 'test3@test.com', 'a093acfcbec5bb016f5afd26f7c701bf227fe714945edc0854317fc486244869071e067cc739799937bf6ee89b72a2e1d59d83e65a7cf196f86ecfc4ea04137c', 'd07d32eeb670b5c6e8326f49583c5414513575bf3c49111d02f19806966364ca336c6edcaac45d7329abc278012d705c098821619e841b086970271ad03aed6d', '0 (2).jpg'),
(9, 'test', 'test@test.com', '9ffa04636edfd8b52e16864216ce6faeb0b0236ce6fc507f0cf95728156657f6963639da0e89ace06df38266817cdb6d2eac0d6f0bec5046a0a14326cdfb6110', 'f43e88b2bdfd21743851646084e8712f5c6ef99ab1b6444d6fd1973d66ccc94ea8befafa3572c496124ed9288f9b430512c3939d09926bcd995454829f3ee5b3', '_MG_0364.JPG'),
(10, 'test33', 'test33@test.com', '67f855bf32605dab01c9a9193e91f5328ba8a30fd07d7ee587aa53f0f85fe1241509c35f8febaa20b3a625463252f206a7c1b243572544d156779fc35af0fbb9', '870c36db618a29feb94ec66810b5404556c16035b29a36b5d85e75b434c666482be26fbce0f6440833fd79cd7b535e5e2558391115757a127a8ff7d5b7bfb5d6', 'calendar1.PNG'),
(11, 'test333', 'test333@test.com', '7239602b99457670efa7096d8300e238641a4139b51218444ba99428eb941f115802374d708c867a0ab5ff0a884097bc104952ffc16490ba4ace28ecfe556082', '3080de19562b63e1f9fef0aa964cd1318212e90e6fcdba8a93365dc2c2b2d5cd526dd2001ce22c9149f432d6080ded3b376811b4611719022a7b3ccc272fc9b5', 'blank.png'),
(12, 'Guest', '', '', '', 'blank.png');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `members`
--
ALTER TABLE `members`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `members`
--
ALTER TABLE `members`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
