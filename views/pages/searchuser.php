<!DOCTYPE html>
<html>
<head>
  <title>profile</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
.container{
  margin-top: auto;
}
.jumbotron{
  box-shadow: 5px 5px 4px grey;
}
.well{
  box-shadow: 5px 5px 5px grey;
}
.dropdown{
  float: right;
}
li .dropdown {list-style-type: none;
}
.right{
  float: right;
}
#img{
  width: 100%;
  height: 15%;
  object-fit: cover;
  margin: auto;
  }

  </style>
</head>
<body>
  <div class="container">

<!-- search user -->
<?php foreach ($users as $user ): ?>
  <div class="well">
    <h3><img src="img/<?php echo $user->img;?>" class="img-circle" alt="" height="50px;" width="50px;">
<a href="?controller=posts&action=profile&username=<?php echo $user->username; ?>"><?php echo $user->username; ?></a></h3>
<span style="color:gray;"><?php echo "email : ".$user->email; ?></span>
<br>
<br>
</div>
<?php endforeach; ?>
<!-- end searchuser -->


<!-- search from post -->
<?php foreach($posts as $post) { ?>
  <div class="well">

        <div class="center text-center">
    <?php if(isset($_SESSION['username'])){ ?>
      <?php if($post->author==$_SESSION['username']) {?>
        <div class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-menu-down"></b></a>
        <ul class="dropdown-menu">
            <li><a href="?controller=posts&action=form&id=<?php echo $post->id;?>">Edit</a></li>
            <li><a href="?controller=posts&action=deletepost&id=<?php echo $post->id;?>">Delete</a></li>
        </ul>
    </div>
    <?php  }?>
    <?php  }?>
    <a href="?controller=posts&action=show&id=<?php echo $post->id; ?>"><b><h3><?php echo $post->title; ?></h3></b></a>
      </div>
      <br>
      <center>
      <a href="?controller=posts&action=show&id=<?php echo $post->id; ?>"><img id="img" src="img/<?php echo $post->img; ?>" alt="" width="75%" ></a>
    </center>
      <br>
      <br>
      <div style=" text-overflow: ellipsis; overflow: hidden; width:80%; white-space:nowrap;  "><?php echo $post->content; ?></div>
      <a href="?controller=posts&action=show&id=<?php echo $post->id; ?>">...Read More</a>
      <br>
      cetagory : <?php echo $post->cetagory; ?><br>
      <?php
      if(isset($_SESSION['username'])){
        if(Post::checklike($post->id,$_SESSION['username'])){
          $action = "like";
        } else{
          $action = "dislike";
      }?>
      <button class="btn-like" type="button" name="button"action="<?php echo $action; ?>" post_id="<?php echo $post->id?>"><?php echo $action; ?> </button>
      <?php } ?>
      <span id="numberlike<?php echo $post->id;?>" n="<?php  echo $post->likes; ?>"><?php  echo $post->likes; ?></span> like
     <div class="right"><b>post by <a href='?controller=posts&action=profile&username=<?php echo $post->author; ?>'><?php echo $post->author; ?></a> <?php echo date('d-m-Y H:i',strtotime($post->time)); ?></b></div>
      </div>

      <?php }?>
