<link rel="stylesheet" href="./css/nav.css"> <!-- navbar stylesheet -->

  <nav class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
      <!-- Brand and toggle get grouped for better mobile display -->
      <div class="navbar-header">
        <button type="button" class="navbar-toggle collapsed" data-toggle="collapse">
          <span class="sr-only">Toggle navigation</span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
          <span class="icon-bar"></span>
        </button>
        <a class="navbar-brand" href="?controller=posts&action=allpost"><span class="glyphicon glyphicon-pushpin"></span> Sotory</a>
        <div class="col-xs-6 col-md-4">
          <form method="get" role="search">
          <div id="custom-search-input">
            <div class="input-group col-md-12">

              <input type="hidden" name="controller" value="pages">
              <input type="hidden" name="action"  value="search">

              <input type="text" class="form-control input-lg" name="search" placeholder="Do you find something ?" required="" />
              <span class="input-group-btn">
                <button class="btn btn-info btn-lg" type="submit">
                  <i class="glyphicon glyphicon-search"></i>
                </button>

              </span>
            </div>
          </div>
        </form>
        </div>

      <!-- Collect the nav links, forms, and other content for toggling -->
      <div id="navbar3" class="navbar-collapse collapse">
        <ul class="nav navbar-nav navbar-right">
          <li><a href="?controller=posts&action=allpost">Home</a></li>
          <?php if (!isset($_SESSION['username'])): ?>
            <li><a data-toggle="modal" data-target="#myModal" href="!#">Login</a></li>
          <?php endif; ?>
          <?php if (isset($_SESSION['username'])): ?>
          <li><a href="?controller=posts&action=index">Following</a></li>
          <!--<li><a href="?controller=posts&action=form">New Story</a></li>-->
          <li><a data-toggle="modal" data-target="#myModal1" href="!#">New Story</a></li>
          <!-- ช่องแจ้งเตือน สำหรับการfollow ถ้าคำที่เราfollow โพส post ให้ มัน จะขึ้นตรงช่องนี้  -->
            <?php $postnoti=Post::noti();?>
            <li><a href="?controller=posts&action=noti"><span class="badge"><?php echo sizeof($postnoti); ?></span></a></li>
          <?php endif; ?>
          <?php if(isset($_SESSION['username'])){ ?>
         <li >
                <a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Hello <?php echo $_SESSION['username']; ?> <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="?controller=posts&action=profile">Profile</a></li>
                  <li><a href="#">Messages</a></li>
                  <li role="separator" class="divider"></li>
                  <li><a href="login/includes/logout.php">Logout</a></li>
                </ul>
              </li>


          <?php } ?>
        </ul>
    </div>
    </div>
  </div>
</div>
</nav>
<?
include_once 'login/includes/db_connect.php';
include_once 'login/includes/functions.php';

sec_session_start();

}
?>
<link rel="stylesheet" href="login/styles/main.css" />
<script type="text/JavaScript" src="login/js/sha512.js"></script>
<script type="text/JavaScript" src="login/js/forms.js"></script>
<div class="modal fade" id="myModal" role="dialog">
    <div class="modal-dialog modal-sm" style="left:0px;">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h4 class="modal-title" style=" text-align: center;">LOGIN TO SOCIAL STORY</h4>
        </div>
        <div class="modal-body" style=" text-align: center;">
            <form action="login/includes/process_login.php" method="post" name="login_form">
            <input class="input" type="text" name="email" placeholder="Email" /><br><br>
            <input class="input" type="password"
                             name="password"
                             id="password" placeholder="Password" /><br><br>
      <div class="modal-footer" style="text-align: center;">
            <!--<input type="button" value="Login" onclick="formhash(this.form, this.form.password);" />-->
            <button type="submit" value="Login" class="btn btn-success" style="width:35%;" data-dismiss="modal" onclick="formhash(this.form, this.form.password);" >Login</button>
          <button type="button" class="btn btn-default" style="width:20%;" data-dismiss="modal">Close</button>
        </div>
        </form>
            <p>If you don't have a login, please <a href="login/register.php">register</a></p>
        </div>
      </div>
    </div>
  </div>

<div class="modal fade" id="myModal1" role="dialog">
    <div class="modal-dialog modal-lg" style="left:0px;width:80%;">
        <div class="modal-content">
            <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body" style=" text-align: center;">
        <?php include("views/posts/form.php"); ?>
            </div>
        </div>
    </div>
</div>
