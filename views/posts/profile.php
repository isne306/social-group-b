
<!DOCTYPE html>
<html>
<head>
  <title>profile</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
  body{
    background: linear-gradient(to left, #00ffff 28%, #99ff99 76%);
    background-size: 100%;
  }
.container{
  margin-top: auto;

}
.jumbotron{
  box-shadow: 5px 5px 4px grey;

}
.well{
  box-shadow: 5px 5px 5px grey;
}
.dropdown{
  float: right;
}
li .dropdown {list-style-type: none;
}
.right{
  float: right;
}
#img{

  object-fit: cover;
  margin: auto;
  }
.pro_name{
  font-size: 2em !important;
}
  </style>
</head>
<body>
<?php
    if(isset($profile)){
        $img= Post::getimgProfile($_GET['username']);
        }
    else{
      $img= Post::getimgProfile('myprofile');
    }
        ?>

  <div class="container">
  <div class="jumbotron text-center">
      <center><img id="img" src="img/<?php echo $img?>" class="img-circle" alt="Cinque Terre" width="180" height="180"></center>

      <?php
      if(isset($profile)){?>
      <center><h1><?php echo $_GET['username']; ?></h1></center>
      <?php if(isset($_SESSION['username'])){
        $follower=follow::getfollower($_GET['username']);
        $following= follow::getfollowing($_GET['username']);
      if(Post::checkfollow($_GET['username'])){
        $action='follow';
      }else {
        $action='unfollow';
       }?>
      <button class="btn btn-primary btn-lg btn-follow" username="<?php echo $_GET['username']?>"><?php echo $action;?></button>
<?php }}else{//myprofile?>
  <?php $follower=follow::getfollower($_SESSION['username']);
    $following=follow::getfollowing($_SESSION['username']);?>
    <p class="pro_name"><?php echo $_SESSION['username']; ?></p>
  <a href="?controller=profile&action=formeditPF"><button type="button" name="button" class="btn btn-primary btn-lg">Edit profile</button></a>
 <?php }?>
<p class="pro_follow"><span class="nfollower"><?php echo $follower; ?></span> follower | <?php echo $following; ?> following</p>



  </div>
  <?php foreach($posts as $post) { ?>
    <div class="well">
        <div class="center text-center">
    <?php if(isset($_SESSION['username'])){ ?>
      <?php if($post->author==$_SESSION['username']) {?>
        <div class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-menu-down"></b></a>
        <ul class="dropdown-menu">
            <li><a href="?controller=posts&action=form&id=<?php echo $post->id;?>">Edit</a></li>
            <li><a href="?controller=posts&action=deletepost&id=<?php echo $post->id;?>">Delete</a></li>
        </ul>
    </div>

    <?php }}?>
    <a href="?controller=posts&action=show&id=<?php echo $post->id; ?>"><b><h3><?php echo $post->title; ?></h3></b></a>
      </div>
      <br>
      <center>
      <a href="?controller=posts&action=show&id=<?php echo $post->id; ?>"><img style="  height: 20%;
        width: 100%;" id="img" src="img/<?php echo $post->img; ?>" alt="" width="75%" ></a>
    </center>
      <br>
      <br>
      <div style="text-overflow: ellipsis; overflow: hidden; width:100%; white-space:nowrap;"><?php echo $post->content; ?></div>
      <a href="?controller=posts&action=show&id=<?php echo $post->id; ?>">...Read More</a>
      <br>

      <?php
      if(isset($_SESSION['username'])){
        if(Post::checklike($post->id,$_SESSION['username'])){
          $action = "like";
        } else{
          $action = "dislike";
      }?>
      <button class="btn-like btn-primary btn-xs" type="button" name="button"action="<?php echo $action; ?>" post_id="<?php echo $post->id?>"><?php echo $action; ?> </button>
      <?php } ?>
      <span id="numberlike<?php echo $post->id;?>" n="<?php  echo $post->likes; ?>"><?php  echo $post->likes; ?></span> like
     <div class="right">
       <b><?php echo $post->cetagory; ?> | by <a href='?controller=posts&action=profile&username=<?php echo $post->author; ?>'><?php echo $post->author; ?></a> <?php echo date('d-m-Y H:i',strtotime($post->time)); ?></b></div>
      </div>

      <?php }?>
      </div>
