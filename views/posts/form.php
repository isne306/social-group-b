
<?php if(isset($_GET['id'])&&$_GET['action']=='form'){
  $post=Post::find($_GET['id']);
  $title=$post->title;
  $content=$post->content;
  $action='editpost';
} else{
  $title='';
  $content='';
  $action='addpost';
}?>

<center>
<div class="form">
<form action="?controller=posts& action=<?php echo $action;?>" method="post" enctype="multipart/form-data">
  <?php if(isset($_GET['id'])){?>
    <input type="hidden" name="id" value="<?php echo $post->id ?>">

<?php }?>
  <input type="hidden" name="author" value="<?php echo $_SESSION['username']; ?>">
  <input class="title" type="text" name="title" value="<?php echo $title ?>" placeholder="title" required=""><br>
  <textarea type="text" name="content" value="" required=""rows="15px"><?php echo $content ?></textarea>
  <?php if(isset($_GET['id'])){?>
    <div class="">
      <img src="img/<?php echo $post->img;?>" style="object-fit:cover;max-width:50%;max-height:50%;"alt="">
    </div>
  <?php } ?>
  <br>Select image to upload:
      <input class="w3-btn w3-white w3-border w3-border-green w3-round-xlarge" type="file" name="fileToUpload" id="fileToUpload">
      <select  class="cetagory"name="cetagory" onChange="onSelectChange()" required="">
        <?php if(isset($_GET['id'])&&$_GET['action']=='form'){?>
          <option value='<?php echo $post->cetagory; ?>'><?php echo $post->cetagory; ?></option>
      <?php  }?>
        <option value='General'>General</option>
        <option value='Sport'>Sport</option>
        <option value='Love'>Love</option>
        <option value='Horror'>Horror</option>
        <option value='others'>others</option>
        </select>
    <div class="modal-footer" style="text-align: center;">
  <input class="btn btn-success btn-md" type="submit" name="submit" value="submit" style="width:70%;" >
        <?php if(!isset($_GET["id"])){ ?>
        <button type="button" class="btn btn-default btn-md" style="width:20%;" data-dismiss="modal">Close</button>
        <?php } ?>
    </div>
</form>
</div>
</center>
<style>
.title, .cetagory,textarea {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

.bt-sm {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

.bt-sm:hover {
    background-color: #45a049;
}

.form{
    width: 80%;
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}
</style>
