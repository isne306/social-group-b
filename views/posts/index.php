<!DOCTYPE html>
<html>
<head>
  <title>Sotory</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style>
  body{
    /*background: linear-gradient(to left, #00ffff 28%, #99ff99 76%);*/
    background-size: 100%;
  }
.container{
  margin-top: auto;
}
.jumbotron{
  box-shadow: 7px 7px 4px grey;

}
.well{
  padding-left: none;
  padding-right: none;
  box-shadow: 5px 5px 5px grey;
  background-color: rgb(209, 254, 194);
}
.dropdown{
  float: right;
}
li .dropdown {
  list-style-type: none;
}
.right{
  float: right;
}
.center{
   float: center;
  }
img {
    width: 100%;
    height: 25%;
    border: 1px solid #ddd;
    border-radius: 4px;
    padding: none;
}
  #img{
  object-fit: cover;
  margin: auto;
  }
  </style>
</head>
<body>
    <br>
<ul class="nav nav-pills nav-justified">
  <li><a href="?controller=posts&action=index&cetagory=General" required="">General</a></li>
  <li><a href="?controller=posts&action=index&cetagory=Sport" required="">Sport</a></li>
  <li><a href="?controller=posts&action=index&cetagory=Love" required="">Love</a></li>
  <li><a href="?controller=posts&action=index&cetagory=Horror" required="">Horror</a></li>
  <li><a href="?controller=posts&action=index&cetagory=others" required="">others</a></li>
</ul>
<br>
<?php foreach($posts as $post) { ?>
  <div class="container">
    <div class="well">
        <div class="center text-center">
    <?php if(isset($_SESSION['username'])){ ?>
      <?php if($post->author==$_SESSION['username']) {?>
        <div class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-menu-down"></b></a>
        <ul class="dropdown-menu">
            <li><a href="?controller=posts&action=form&id=<?php echo $post->id;?>">Edit</a></li>
            <li><a href="?controller=posts&action=deletepost&id=<?php echo $post->id;?>">Delete</a></li>
        </ul>
    </div>
    <?php  }?>
    <?php  }?>
    <a href="?controller=posts&action=show&id=<?php echo $post->id; ?>"><b><h3><?php echo $post->title; ?></h3></b></a>
      </div>
      <br>
      <center>
      <a href="?controller=posts&action=show&id=<?php echo $post->id; ?>"><img id="img" src="img/<?php echo $post->img; ?>" alt="" width="75%" ></a>
    </center>
      <br>
      <br>
      <div style="text-overflow: ellipsis; overflow: hidden; width:100%; white-space:nowrap;"><?php echo $post->content; ?></div>
      <a href="?controller=posts&action=show&id=<?php echo $post->id; ?>">...Read More</a>
      <br>

      <?php
      if(isset($_SESSION['username'])){
        if(Post::checklike($post->id,$_SESSION['username'])){
          $action = "like";
        } else{
          $action = "dislike";
      }?>
      <button class="btn-like btn-primary btn-xs" type="button" name="button"action="<?php echo $action; ?>" post_id="<?php echo $post->id?>"><?php echo $action; ?> </button>
      <?php } ?>
      <span id="numberlike<?php echo $post->id;?>" n="<?php  echo $post->likes; ?>"><?php  echo $post->likes; ?></span> like
     <div class="right">
       <b><?php echo $post->cetagory; ?> | by <a href='?controller=posts&action=profile&username=<?php echo $post->author; ?>'><?php echo $post->author; ?></a> <?php echo date('d-m-Y H:i',strtotime($post->time)); ?></b></div>
      </div>
    </div>
      <?php }?>
</body>
</html>
