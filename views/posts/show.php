<!DOCTYPE html>
<html>
<head>
  <title>profile</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <!-- <link rel="stylesheet" type="text/css" href="profile.php"> -->
  <style>
    .container#post{
      margin-top: 20px;
      border: 1px solid #e3e3e3;
      overflow: hidden;
      background-color: #f5f5f5;
    }
    img{
      object-fit: fill;
      height: 50%;
      width: 100%;
    }
    .comment{
      /*border: 1px solid #e3e3e3;*/

    }
    .com_form{
      /*border: 1px solid #e3e3e3;*/
    }
    .com_con#cont1{
      font-size: 1.9em;
    }
    #title{
      font-size: 3em;
      text-align: center;
    }
    #author{
      font-size: 1em;
      text-align: center;
      color: blue;
    }
    #content{
      font-size: 1.5em;
    }
    .dropdown{
  float: right;
}
li .dropdown {list-style-type: none;
}
  .com_from_con{
      width: 100%;
      height: 5%;
      border-radius: 5px;
      font-size: 1.8em;
    }
    .form-control#com_btn{
      width: 100%;
    }
  </style>
</head>
<body>
<!-- <p>This is the requested post:</p> -->
<div class="container"><br>
<div class="well">
  <?php if(isset($_SESSION['username'])){ ?>
      <?php if($post->author==$_SESSION['username']) {?>
        <div class="dropdown">
        <a href="#" class="dropdown-toggle" data-toggle="dropdown"><b class="glyphicon glyphicon-menu-down"></b></a>
        <ul class="dropdown-menu">
            <li><a href="?controller=posts&action=form&id=<?php echo $post->id;?>">Edit</a></li>
            <li><a href="?controller=posts&action=deletepost&id=<?php echo $post->id;?>">Delete</a></li>
        </ul>
    </div>
  <?php  }}?>
  <p id="title"><?php echo $post->title; ?></p>
  <a id="author" href=.?controller=posts&action=profile&username=<?php echo $post->author; ?>><center><?php echo $post->author; ?></center></a>
  <hr>
  <br>
  <div class="img_p">
    <img src="img/<?php echo $post->img; ?>">
  </div>
  <br>
  <?php if(isset($_SESSION['username'])){
    if(Post::checklike($post->id,$_SESSION['username'])){
      $action = "like";
    } else{
      $action = "dislike";
    }?>
      <button class="btn-like btn-primary btn-xs" type="button" name="button"action="<?php echo $action; ?>" post_id="<?php echo $post->id?>"><?php echo $action; ?></button>
      <?php } ?>

  <span id="numberlike<?php echo $post->id;?>" n="<?php  echo $post->likes; ?>"><?php  echo $post->likes; ?></span> like
  <br>
  <hr>

  <p id="content" style="word-break: break-all;"><?php echo $post->content; ?></p>
  <hr>
  <?php if(isset($_SESSION['username'])){
    $commenter=$_SESSION['username'];
  } else{
    $commenter='Guest';
  }?>
  <!-- comment -->
    <form class="com_form" action="?controller=posts& action=addcomment& id=<?php echo $_GET['id'];?>" method="post">
    <?php //echo $commenter;?>
      <input class="form-control" id="com_btn" type="text" name="comment" value="">
      <input style="float:right;" class="btn btn-primary" type="submit" name="submit" value="comment">
      <input type="hidden" name="username" value="<?php echo $commenter;?>"><br>
      <hr>

  </form>

  <div class="comment">
  <?php foreach($comments as $comment) { ?>
    <?php $img = Post::getimgProfile($comment->username) ?>
    <p class="com_con" id="cont1"><img src="img/<?php echo $img;?>" class="img-circle" style="width:50px !important;height:50px !important; object-fit: cover;"alt=""><?php echo " ".$comment->comment ?></h2></p>
    <p style="color:gray"><a href="?controller=posts&action=profile&username=<?php echo $comment->username ?>"><?php echo $comment->username ?></a>

    | <?php echo date('d-m-Y H:i',strtotime($comment->time)); ?></p>
    <hr>
    <?php } ?>

  </div>
</div>
</div>
