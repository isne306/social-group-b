<!DOCTYPE html>
<html>
<head>
	<title>Edit Profile</title>
	<meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  <style type="text/css">
.title, .cetagory,textarea {
    width: 100%;
    padding: 12px 20px;
    margin: 8px 0;
    display: inline-block;
    border: 1px solid #ccc;
    border-radius: 4px;
    box-sizing: border-box;
}

.bt-sm {
    width: 100%;
    background-color: #4CAF50;
    color: white;
    padding: 14px 20px;
    margin: 8px 0;
    border: none;
    border-radius: 4px;
    cursor: pointer;
}

.bt-sm:hover {
    background-color: #45a049;
}

.form{
	margin: auto;
    width: 80%;
    border-radius: 5px;
    background-color: #f2f2f2;
    padding: 20px;
}
.container{
  margin-top: auto;

}
.jumbotron{
  box-shadow: 5px 5px 4px grey;

}
  </style>
</head>
<body>
	<div class="container">
	<div class="form">
		<center><img id="img" src="img/<?php echo $profile->img;?>" class="img-circle" alt="Cinque Terre" width="180" height="180"></center>
<form action="?controller=profile&action=editprofile" method="post" enctype="multipart/form-data"><br>
  <center>Select image to upload:
      <input class="w3-btn w3-white w3-border w3-border-green w3-round-xlarge" type="file" name="fileToUpload" id="fileToUpload"></center>
  <input class="bt-sm" type="submit" name="submit" value="submit" >
</form>
</div>
</div>
</body>
</html>
