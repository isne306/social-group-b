<?php
  function call($controller, $action) {
    require_once('controllers/' . $controller . '_controller.php');

    switch($controller) {
      case 'pages':
      require_once('models/page.php');
      require_once('models/post.php');

        $controller = new PagesController();
      break;
      case 'posts':
        // we need the model to query the database later in the controller
        require_once('models/post.php');
        require_once('models/follow.php');

        $controller = new PostsController();
      break;
      case 'profile':
        // we need the model to query the database later in the controller
        require_once('models/profile.php');
        $controller = new ProfileController();
      break;
      case 'follow':
        // we need the model to query the database later in the controller
        require_once('models/follow.php');
        $controller = new followController();
      break;
    }

    $controller->{ $action }();
  }

  // we're adding an entry for the new controller and its actions
  $controllers = array('pages' => ['home', 'error','search'],
                       'posts' => ['index', 'show','form','addpost',
                                    'addcomment','like','dislike',
                                    'profile','allpost','editpost',
                                    'deletepost','noti'
                                   ],
                      'profile'=> ['editprofile','formeditPF'],
                      'follow'=>['follow','unfollow']);

  if (array_key_exists($controller, $controllers)) {
    if (in_array($action, $controllers[$controller])) {
      call($controller, $action);
    } else {
      call('pages', 'error');
    }
  } else {
    call('pages', 'error');
  }
?>
