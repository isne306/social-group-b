<?php
  class PostsController {
    public function index() {
      // we store all the posts in a variable
      if(isset($_GET['cetagory'])){
        $posts=Post::feedcategory($_GET['cetagory']);
        // echo "<br>cetagory :  ".$_GET['cetagory'];
      }else if (isset($_SESSION['id'])){
          $posts=Post::feeds($_SESSION['id']);
      }
      else{
        if(isset($_SESSION['user_id'])){
          $posts = Post::allfollow();
        }
        // else{
        //   $posts=Post::allpost();
        // }
      }
      require_once('views/posts/index.php');
    }
    public function allpost()
    {
     $posts=Post::allpost();
     require_once('views/posts/index.php');
    }

    public function profile() {
      // we store all the posts in a variable
      if(isset($_SESSION['username'])){
        $session=$_SESSION['username'];
      }
      else {
        $session='';
      }
      if(isset($_GET['username'])&&$_GET['username']!=$session){
        $posts=Post::profile($_GET['username']);
        $profile = $_GET['username'];
      }else {
          $posts = Post::profile('myprofile');

    }
    require_once('views/posts/profile.php');

  }

      public function noti()
      {
        $posts = Post::noti();
        require_once('views/posts/index.php');

      }
    public function show() {
      // we expect a url of form ?controller=posts&action=show&id=x
      // without an id we just redirect to the error page as we need the post id to find it in the database
      if (!isset($_GET['id']))
        return call('pages', 'error');

      // we use the given id to get the right post
      $post = Post::find($_GET['id']);
      $comments = comment::getcomment($_GET['id']);
      Post::read($_GET['id']);
      require_once('views/posts/show.php');
    }

    public function form()
    {
      if(isset($_GET['id'])){
      $post = Post::find($_GET['id']);
      }
      require_once('views/posts/form.php');
    }
    public function addpost()
    {
      Post::addpost($_POST['author'],$_POST['title'],$_POST['content'],basename($_FILES['fileToUpload']['name']),$_POST['cetagory']);
     header ("Location: http://localhost/social/?controller=posts&action=allpost");
    }
    public function addcomment()
    {
      comment::addcomment($_GET['id'],$_POST['username'],$_POST['comment']);
      $id=$_GET['id'];
      header ("Location: http://localhost/social/?controller=posts&action=show&id=$id");

    }
    public function editpost()
    {
      echo basename($_FILES['fileToUpload']['name']);
      if(basename($_FILES['fileToUpload']['name'])==''){
        $img='';
      }else{
        $img=basename($_FILES['fileToUpload']['name']);
      }
      Post::editpost($_POST['id'],$_POST['title'],$_POST['content'],$img,$_POST['cetagory']);
      $id=$_POST['id'];
      header("Location: ?controller=posts&action=show&id=$id");
    }
    public function deletepost()
    {
      Post::deletepost($_GET['id']);
      header("Location: ?controller=posts&action=allpost");
    }
    public function like()
    {
      Post::like($_GET['id'],$_SESSION['username']);
      header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
    public function dislike()
    {
      Post::dislike($_GET['id'],$_SESSION['username']);
      header('Location: ' . $_SERVER['HTTP_REFERER']);
    }
  }
?>
