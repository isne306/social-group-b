<?php
  class PagesController {
    public function home() {
      if(isset($_SESSION['username']))
      $first_name = $_SESSION['username'];
      require_once('views/pages/home.php');
    }

    public function error() {
      require_once('views/pages/error.php');
    }
    public function search()
    {
      $users=Page::searchuser($_GET['search']);
      $posts=Post::searchpost($_GET['search']);
      require_once('views/pages/searchuser.php');
    }
  }
?>
