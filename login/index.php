<?php
/**
 * Copyright (C) 2013 peredur.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

include_once 'includes/db_connect.php';
include_once 'includes/functions.php';

sec_session_start();

if (login_check($mysqli) == true) {
    $logged = 'in';
} else {
    $logged = 'out';
}
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Secure Login: Log In</title>
        <link rel="stylesheet" href="styles/main.css" />
        <script type="text/JavaScript" src="js/sha512.js"></script>
        <script type="text/JavaScript" src="js/forms.js"></script>
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    </head>
    <body>


        <div class="centerdiv">
            <div class="divform">
                <br>
                <h2>LOGIN TO SOCIAL STORY</h2>
                <hr class="hr"><br>
        <form action="includes/process_login.php" method="post" name="login_form">
            <input class="input" type="text" name="email" placeholder="Email" /><br><br>
            <input class="input" type="password"
                             name="password"
                             id="password" placeholder="Password" /><br><br>
            <input class="bt-sm" type="button"
                   value="Login"
                   onclick="formhash(this.form, this.form.password);" /><br>
        </form>
        <hr class="hr">
        <p>If you don't have a login, please <a href="register.php">register</a></p>
        <p><a href="../">go HOME</a>.</p>
        <?php

        if (isset($_GET['error'])) {
            echo '<p class="error">Error Logging In!</p>';
        }
        ?>
            </div>
        </div>

    </body>
</html>
