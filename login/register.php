<?php
/**
 * Copyright (C) 2013 peredur.net
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */
include_once 'includes/register.inc.php';
include_once 'includes/functions.php';
?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Secure Login: Registration Form</title>
        <script type="text/JavaScript" src="js/sha512.js"></script>
        <script type="text/JavaScript" src="js/forms.js"></script>
        <script type="text/javascript" src="js/passmeter.js"></script>
        <link rel="stylesheet" href="styles/main.css" />
        <link rel="stylesheet" href="http://www.w3schools.com/lib/w3.css">
    </head>
    <body>
        <!-- Registration form to be output if the POST variables are not
        set or if the registration script caused an error. -->
        <div class="centerregister">
        <h1>Register with us</h1>
            <hr class="hr">
        <?php
        if (!empty($error_msg)) {
            echo $error_msg;
        }
        ?>
        <ul>
            <li>Usernames may contain only digits, upper and lower case letters and underscores</li>
            <li>Emails must have a valid email format</li>
            <li>Passwords must be at least 6 characters long</li>
            <li>Passwords must contain
                <ul>
                    <li>At least one upper case letter (A..Z)</li>
                    <li>At least one lower case letter (a..z)</li>
                    <li>At least one number (0..9)</li>
                </ul>
            </li>
            <li>Your password and confirmation must match exactly</li>
        </ul>
            <hr class="hr">
        <form method="post" name="registration_form" action="<?php echo esc_url($_SERVER['PHP_SELF']); ?>" enctype="multipart/form-data">
        <!-- <form method="post" name="registration_form" action="test.php" enctype="multipart/form-data"> -->
            <input class="input" type='text' name='username' id='username' placeholder="Username" /><br><br>
            <input class="input" type="text" name="email" id="email" placeholder="Email" /><br><br>
            <input class="input" type="password" name="password" id="password" placeholder="Password" onkeyup="check_password(document.forms.registration_form.password.value)" />
            <!-- pwd_bar is where the colored strength bar appears -->
            <div class="meterpassdiv">
            <div id="pwd_bar"></div>
            </div>
            <!-- pwd_meter is where the strength word appears, like "weak" -->
            <span id="pwd_meter"></span>

            <!-- pwd_log is the section that debug information appears if you set DEBUG=1 above -->
            <span id="pwd_log"></span><br>
            <input class="input" type="password" name="confirmpwd" id="confirmpwd" placeholder="Confirm password" /><br>
            <br>Select profile image:
            <input class="w3-btn w3-white w3-border w3-border-green w3-round-xlarge" type="file" name="fileToUpload" id="fileToUpload"><br><br>
            <hr class="hr">

            <input class="bt-sm" type="button"
                   value="Register"
                   onclick="return regformhash(this.form,
                                   this.form.username,
                                   this.form.email,
                                   this.form.password,
                                   this.form.confirmpwd,
                                   this.form.fileToUpload);" />
        </form>
        <p>Return to the <a href="../index.php">Home page</a>.</p>
        </div>
    </body>
</html>
